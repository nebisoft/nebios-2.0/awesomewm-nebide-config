local awful = require('awful')
local beautiful = require("beautiful")
local dpi = beautiful.xresources.apply_dpi
local s = awful.screen.focused()
local geo = s.geometry
local lp = s.left_panel
local tp = s.top_panel
local action_bar_width = dpi(10 + beautiful.action_bar_icon_size)

local changes = function()
	lp.width = action_bar_width
	lp.y = geo.y
	lp.x = geo.x
	lp.height = geo.height
	lp.shape = nil
	lp:struts
	{
		left = action_bar_width
	}
	lp:set_xproperty("WM_NAME", "NebiDE_left2")
	tp.stretch = false
	tp.margins = nil
	tp.width = geo.width - action_bar_width
	tp.y = geo.y
	tp.x = geo.x + action_bar_width
	tp.shape = nil
	tp:struts
	{
		top = dpi(28)
	}
	tp:set_xproperty("WM_NAME", "NebiDE_top2")
	os.execute("bash -c 'sleep 1; feh --bg-scale $(shuf -n1 -e $HOME/.config/awesome/theme/wallpapers/*.{png,jpg})' &")
	
	local lang = os.getenv("LANG")

	-- Locales
	local title =  'Congratulations!'
	local easter =  "We're have no idea how did you find this easter egg but, enjoy the early 2020's NebiDE.\n\nDon't worry, restart the computer to revert changes."

	if lang == "tr_TR.UTF-8" then
		title =  'Tebrikler!'
		easter = "Bunu nasıl bulduğunu bilmiyoruz ancak NebiDE'nin erken 2020 sürümünün tadını çıkart.\n\nMerak etmeyin, bilgisayarı yeniden başaltırsanız değişiklikler geri alınacaktır."
	end
	os.execute("notify-send \"" .. title .."\" \"" .. easter .. "\" &")
end

changes()
