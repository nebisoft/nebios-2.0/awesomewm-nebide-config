return function(menu)
	client.focus = nil
		
	client.connect_signal("focus", function(c)
		menu:hide()
	end)
	
	awesome.connect_signal("client_killed", function(c)
		if c.class == "client" and c.valid == false then
			menu:hide()
		end
	end)
	
	menu:show()
end
