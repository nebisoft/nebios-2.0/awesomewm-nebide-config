local modkey = require('configuration.keys.mod').mod_key
local awful = require('awful')
local altkey = require('configuration.keys.mod').alt_key

local function check_border(mx, my, cx, cy, cw, ch)
	  local onborder = nil
	  if my > (cy + ch - 6) then
		    if mx < (cx + 30) then onborder = "bottom_left"
		    elseif mx > (cx + cw - 30) then onborder = "bottom_right"
		    else onborder = "bottom"
		    end
	  elseif mx < (cx + 4) then onborder = "left"
	  elseif mx > (cx + cw - 4) then onborder = "right"
	  end
	  return onborder
end

return awful.util.table.join(
	awful.button(
		{},
		1,
		function(c)
			c:emit_signal('request::activate')
			c:raise()
			if c.requests_no_titlebar == false then
				local cx, cy, cw, ch = c:geometry().x, c:geometry().y, c:geometry().width, c:geometry().height
				local mx, my = _G.mouse.coords().x, _G.mouse.coords().y
				local onborder = check_border(mx, my, cx, cy, cw, ch)
				if onborder then
				    awful.mouse.client.resize(c, onborder)
				end
			end
		end
	),
	awful.button({altkey},1, awful.mouse.client.move),
	awful.button({altkey},3, awful.mouse.client.resize)
)
