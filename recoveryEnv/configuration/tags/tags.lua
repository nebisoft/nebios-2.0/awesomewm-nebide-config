local awful = require('awful')
local beautiful = require('beautiful')
local lookup = require('menubar').utils

return {
	{
		type = [[Internet Tagged Desktop]],
		icon = lookup.lookup_icon("web-browser"),
		default_app = "xdg-open http://",
		gap = beautiful.useless_gap,
		layout = awful.layout.suit.floating
	},
	{
		type = [[Coding Tagged Desktop]],
		icon = lookup.lookup_icon("gnome-builder"),
		default_app = "gedit",
		gap = beautiful.useless_gap,
		layout = awful.layout.suit.floating
	},
	{
		type = [[Files Tagged Desktop]],
		icon = lookup.lookup_icon("nautilus"),
		default_app = "nautilus",
		gap = beautiful.useless_gap,
		layout = awful.layout.suit.tile
	},
	{
		type = [[Multimedia Tagged Desktop]],
		icon = lookup.lookup_icon("totem"),
		default_app = "totem",
		gap = beautiful.useless_gap,
		layout = awful.layout.suit.floating
	},
	{
		type = [[Gaming Tagged Desktop]],
		icon = lookup.lookup_icon("lutris"),
		default_app = "",
		gap = beautiful.useless_gap,
		layout = awful.layout.suit.max
	},
	{
		type = [[Graphics Tagged Desktop]],
		icon = lookup.lookup_icon("krita"),
		default_app = "gimp",
		gap = beautiful.useless_gap,
		layout = awful.layout.suit.floating
	}
}
