local lgi = require("lgi")
local Gio = lgi.Gio

-- IconPathConverter class definition
local IconPathConverter = {}
IconPathConverter.__index = IconPathConverter

function IconPathConverter.new()
    local self = setmetatable({}, IconPathConverter)
    return self
end

function IconPathConverter:to_path(iconName)
    local themedIcon = Gio.ThemedIcon.new(iconName)
    
    if iconInfo then
        return themedIcon:get_filename()
    else
        return nil
    end
end

return IconPathConverter  -- Return the class for external use
