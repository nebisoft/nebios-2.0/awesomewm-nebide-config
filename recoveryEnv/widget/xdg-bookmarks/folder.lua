local wibox = require('wibox')
local awful = require('awful')
local gears = require('gears')
local icons = require('theme.icons')
local clickable_container = require('widget.clickable-container')
local dpi = require('beautiful').xresources.apply_dpi
local utils = require("menubar.utils")
local config_dir = gears.filesystem.get_configuration_dir()
local widget_dir = config_dir .. 'widget/pinned-apps/'

local create_widget = function(folder, xdg_dirs)
	function string:endswith(suffix)
	    return self:sub(-#suffix) == suffix
	end
	
	local function iconsel(thefolder)
		    if thefolder:endswith(xdg_dirs.DESKTOP) then 
			return "/usr/share/icons/Flat-Skeuomorphism/scalable/places/red-user-desktop.svg"
		    elseif thefolder:endswith(xdg_dirs.DOCUMENTS) then
			return "/usr/share/icons/Flat-Skeuomorphism/scalable/places/folder-documents.svg"
		    elseif thefolder:endswith(xdg_dirs.DOWNLOAD) then 
			return "/usr/share/icons/Flat-Skeuomorphism/scalable/places/folder-download.svg"
		    elseif thefolder:endswith(xdg_dirs.MUSIC) then
			return "/usr/share/icons/Flat-Skeuomorphism/scalable/places/folder-music.svg"
		    elseif thefolder:endswith(xdg_dirs.PICTURES) then
			return "/usr/share/icons/Flat-Skeuomorphism/scalable/places/folder-picture.svg"
		    elseif thefolder:endswith(xdg_dirs.PUBLICSHARE) then
			return "/usr/share/icons/Flat-Skeuomorphism/scalable/places/folder-public.svg"
		    elseif thefolder:endswith(xdg_dirs.TEMPLATES) then
			return "/usr/share/icons/Flat-Skeuomorphism/scalable/places/folder-templates.svg"
		    elseif thefolder:endswith(xdg_dirs.VIDEOS) then
			return "/usr/share/icons/Flat-Skeuomorphism/scalable/places/folder-videos.svg"
		    elseif thefolder:endswith("/Users/" .. os.getenv("USER") .. "/NebiCloud") then
			return "/usr/share/icons/Flat-Skeuomorphism/scalable/places/folder-nebicloud.svg"
		    elseif thefolder == "?unknown?" then
			return "/usr/share/icons/Flat-Skeuomorphism/scalable/mimetypes/unknown.svg"
		    elseif thefolder == "!error!" then 
			return "/usr/share/icons/Flat-Skeuomorphism/32/status/dialog-error.svg"
		    elseif thefolder == ".null." then
			return "/usr/share/icons/Flat-Skeuomorphism/scalable/places/red-folder-bookmark.svg" 
		    else
			return "/usr/share/icons/Flat-Skeuomorphism/scalable/places/folder.svg"
		    end
	end
	
	local folder_widget =	wibox.widget {
		{
			image = iconsel(folder),
			resize = true,
			widget = wibox.widget.imagebox
		},
		layout = wibox.layout.align.horizontal
	}

	local folder_button = wibox.widget {
		{
			{
				folder_widget,
				margins = dpi(5),
				widget = wibox.container.margin
			},
			shape = function(cr, width, height)
				gears.shape.rounded_rect(cr, width, height, dpi(12))
			end,
			widget = clickable_container
		},
		margins = dpi(2),
		widget = wibox.container.margin
	}
	
	local tthtml = "<b>" .. folder:gsub(".*/", "") .. "</b> at " .. folder
	
	if folder == ".null." then
		tthtml = "To add an bookmark, drag an folder to the <b>New\nbookmark</b> text on the sidebar in Files app."
	end
	
	local tooltip =  awful.tooltip {
		objects = {folder_button},
		markup = tthtml,
		mode = "outside",
		margin_leftright = dpi(10),
		margin_topbottom = dpi(6),
		preferred_alignments = {'middle'},
		preferred_positions = {'top', 'right', 'left'},
		align = 'top'
	}

	folder_button:buttons(
		gears.table.join(
			awful.button(
				{},
				1,
				nil,
				function()
					local cmd = "xdg-open file://" .. folder .. " &"
					
					if folder == ".null." then
						cmd = "xdg-open $HOME &"
					end
					
					awful.spawn.with_shell(cmd, true)
				end
			)
		)
	)

	return folder_button
end

return create_widget
