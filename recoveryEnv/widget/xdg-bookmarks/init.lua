local awful = require('awful')
local gears = require('gears')
local wibox = require('wibox')
local beautiful = require('beautiful')
local dpi = beautiful.xresources.apply_dpi
local config_dir = gears.filesystem.get_configuration_dir()
local widget_dir = config_dir .. 'widget/xdg-bookmarks/'
local contents = ""

local create_bookmarks = function(lay)
	local separator =  wibox.widget {
		orientation = 'horizontal',
		forced_height = dpi(1),
		forced_width = dpi(1),
		span_ratio = 0.25,
		widget = wibox.widget.separator
	}
	
	widgetbookmark = wibox.widget {
		layout = lay
	}
	
	local xdg_dirs = {}
	local user_dirs_file = os.getenv("HOME") .. "/.config/user-dirs.dirs"

	local file = io.open(user_dirs_file, "r")
	if file then
	    for line in file:lines() do
		local key, value = line:match("XDG_(%w+)_DIR=\"(.+)\"")
		if key and value then
		    value = value:gsub("$HOME", os.getenv("HOME"))
		    xdg_dirs[key] = value
		end
	    end
	    file:close()
	end
		
	local timer2 = gears.timer {
	    timeout   = 5,
	    call_now  = true,
	    autostart = true,
	    callback = function()

		local bookmarkss = {}

		local bookmarks_file = os.getenv("HOME") .. "/.config/gtk-3.0/bookmarks"

		awful.spawn.easy_async("cat " .. bookmarks_file, function(stdout)
		    if contents == stdout then else
			    widgetbookmark:reset()
			    contents = stdout

			    local lines = {}
			    for line in contents:gmatch("[^\r\n]+") do
				table.insert(lines, line)
			    end

			    for _, line in ipairs(lines) do
				local _, _, path = line:find("^file://(.+)$")
				if path then
				    local decoded_path = string.gsub(path, "%%(%x%x)", function(hex)
				        return string.char(tonumber(hex, 16))
				    end)

				    table.insert(bookmarkss, require("widget.xdg-bookmarks.folder")(decoded_path, xdg_dirs))
				end
			    end

			    if not next(bookmarkss) then
				bookmarkss = { require("widget.xdg-bookmarks.folder")(".null.") }
			    end
			 
			    widgetbookmark:add(table.unpack(bookmarkss))
			    collectgarbage()
		    end
		end)
	    end
	}

	
	return widgetbookmark
end

return create_bookmarks
