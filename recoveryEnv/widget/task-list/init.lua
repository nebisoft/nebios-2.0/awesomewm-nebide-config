local awful = require('awful')
local wibox = require('wibox')
local beautiful = require('beautiful')
local dpi = require('beautiful').xresources.apply_dpi
local gears = require('gears')
local ems = require('module.ems')
local clickable_container = require('widget.clickable-container')
local icons = require('theme.icons')
local config_dir = gears.filesystem.get_configuration_dir()
local widget_dir = config_dir .. 'widget/pinned-apps/'

--- Common method to create buttons.
-- @tab buttons
-- @param object
-- @return table
local function create_buttons(buttons, object)
	if buttons then
		local btns = {}
		for _, b in ipairs(buttons) do
			-- Create a proxy button object: it will receive the real
			-- press and release events, and will propagate them to the
			-- button object the user provided, but with the object as
			-- argument.
			local btn = awful.button {
				modifiers = b.modifiers,
				button = b.button,
				on_press = function()
					b:emit_signal('press', object)
				end,
				on_release = function()
					b:emit_signal('release', object)
				end
			}
			btns[#btns + 1] = btn
		end
		return btns
	end
end

local function list_update(w, buttons, label, data, objects)
	-- Update the widgets, creating them if needed
	w:reset()
	for i, o in ipairs(objects) do
		local cache = data[o]
		local ib, cb, tb, cbm, bgb, tbm, ibm, tt, l, ll, bg_clickable
		if cache then
			ib = cache.ib
			tb = cache.tb
			bgb = cache.bgb
			tbm = cache.tbm
			ibm = cache.ibm
			tt  = cache.tt
		else
			ib = wibox.widget.imagebox()
			tb = wibox.widget.textbox()
			cb = wibox.widget {
				{
					{
						image = icons.close,
						resize = true,
						widget = wibox.widget.imagebox
					},
					margins = dpi(4),
					widget = wibox.container.margin
				},
				widget = clickable_container
			}
			cb.shape = gears.shape.circle
			cbm = wibox.widget {
				-- 4, 8 ,12 ,12 -- close button
				cb,
				left = dpi(4),
				right = dpi(8),
				top = dpi(4),
				bottom = dpi(4),
				widget = wibox.container.margin
			}
			cbm:buttons(
				gears.table.join(
					awful.button(
						{},
						1,
						nil,
						function()
							o:kill()
						end
					)
				)
			)
			bg_clickable = clickable_container()
			bgb = wibox.container.background()
			tbm = wibox.widget {
				tb,
				left = dpi(4),
				right = dpi(4),
				widget = wibox.container.margin
			}
			ibm = wibox.widget {
				-- 12 top bottom
				ib,
				left = dpi(6),
				right = dpi(6),
				top = dpi(6),
				bottom = dpi(6),
				widget = wibox.container.margin
			}
			l = wibox.layout.fixed.horizontal()
			ll = wibox.layout.fixed.horizontal()

			-- All of this is added in a fixed widget
			l:fill_space(true)
			l:add(ibm)
			l:add(tbm)
			ll:add(l)
			ll:add(cbm)

			bg_clickable:set_widget(ll)
			-- And all of this gets a background
			bgb:set_widget(bg_clickable)

			l:buttons(create_buttons(buttons, o))

			-- Tooltip to display whole title, if it was truncated
			local tt =  awful.tooltip {
				objects = {ib},
				mode = "outside",
				margin_leftright = dpi(10),
				margin_topbottom = dpi(6),
				preferred_alignments = {'middle'},
				preferred_positions = {'top', 'right', 'left'},
				align = 'top'
			}

			data[o] = {
				ib = ib,
				tb = tb,
				bgb = bgb,
				tbm = tbm,
				ibm = ibm,
				tt  = tt
			}
		end

		local text, bg, bg_image, icon, args = label(o, tb)
		args = args or {}

		-- The text might be invalid, so use pcall.
		if text == nil or text == '' then
			tbm:set_margins(0)
		else
			tt:set_text(text)
		end
		bgb:set_bg(bg)
		if type(bg_image) == 'function' then
			-- TODO: Why does this pass nil as an argument?
			bg_image = bg_image(tb, o, nil, objects, i)
		end
		bgb:set_bgimage(bg_image)
		if icon then
			ib.image = gears.surface(icon)
		else
			ibm:set_margins(0)
		end

		bgb.shape = args.shape
		bgb.shape_border_width = args.shape_border_width
		bgb.shape_border_color = args.shape_border_color

		w:add(bgb)
	end
end

local tasklist_buttons = awful.util.table.join(
	awful.button(
		{},
		1,
		function(c)
				if cl_menu then
        cl_menu:hide()
        cl_menu=nil
    else
        client_num=0
        client_list={}
        for i, cl in pairs(client.get()) do
            if cl.class == c.class then
                client_num = client_num + 1
                client_list[i]=
                  {cl.name .. " · " .. cl.first_tag.name,
                   function()
                       client.focus = cl
                       awful.tag.viewonly(cl:tags()[1])
                       cl:raise()
                   end,
                   cl.icon
                  }
           end
        end

        if client_num > 1 then
            ems(awful.menu({items = client_list, theme = {width=512}}))
        else
            client.focus = c
            awful.tag.viewonly(c:tags()[1])
            c:raise() 
        end
    end
		
		end
	),
	awful.button(
		{},
		2,
		function(c)
			c:kill()
		end
	),
	awful.button({ }, 3, function (c)
					if instance and instance.wibox.visible then
                                              instance:hide()
                                              instance = nil
                                          else
                                              rclick = { { "Pin to Dock", function() os.execute([[
                                              
                                        		nebide-pin ]] .. c.pid .. [[ &
                                        
                                        	]]
                                              	) end, icons.add } }
                                              ems(awful.menu(rclick))
                                          end
                                      end),
	awful.button(
		{},
		4,
		function()
			awful.client.focus.byidx(1)
		end
	),
	awful.button(
		{},
		5,
		function()
			awful.client.focus.byidx(-1)
		end
	)
)

client.connect_signal("manage", function(c)
    local cairo = require("lgi").cairo
    local default_icon = "/usr/share/icons/Fluent-red/scalable/mimetypes/application-x-executable.svg"
    if c and c.valid and not c.icon then
        local s = gears.surface(default_icon)
        local img = cairo.ImageSurface.create(cairo.Format.ARGB32, s:get_width(), s:get_height())
        local cr = cairo.Context(img)
        cr:set_source_surface(s, 0, 0)
        cr:paint()
        c.icon = img._native
    end
end)

local cell = {
{
	{
		    {
			
			    {
			        id     = 'clienticon',
			        widget = awful.widget.clienticon,
			    },
			    margins = dpi(4),
			    widget  = wibox.container.margin,
			
		    },
		    widget = wibox.container.margin
		},
		id     = 'background_role',
		widget = wibox.container.background
	},
	margins = dpi(2),
	widget = wibox.container.margin
}

local tooltip = awful.tooltip {
	objects = {self},
	mode = "outside",
	margin_leftright = dpi(10),
	margin_topbottom = dpi(6),
	preferred_alignments = {'middle'},
	preferred_positions = {'top', 'right', 'left'},
	align = 'top'
}

function cell:create_callback(c, index, clients)
	tooltip:add_to_object(self)
	self:connect_signal("mouse::enter", function()
		tooltip.markup = c.name or "<i>Untitled</i>"
	end)
end

local task_list = function(s, lay)
	-- awful.widget.tasklist(
	--	s,
	--	awful.widget.tasklist.filter.currenttags,
	--	tasklist_buttons,
	--	{},
	--	list_update,
	--	wibox.layout.fixed.horizontal()
	--)
	return awful.widget.tasklist {
    screen = s,
    style = {
    	shape = function(cr, width, height)
		gears.shape.rounded_rect(cr, width, height, dpi(12))
	end
    },
    widget_template = cell,
    buttons = tasklist_buttons,
    layout = lay(),
    filter = function() return true end, -- Filtering is already done in source
    source = function()
        -- Get all clients
        local cls = client.get()

        -- Filter by an existing filter function and allowing only one client per class
        local result = {}
        local class_seen = {}
        for _, c in pairs(cls) do
            if awful.widget.tasklist.filter.currenttags(c, s) then
                if not class_seen[c.class] then
                    class_seen[c.class] = true
                    table.insert(result, c)
                end
            end
        end
        return result
    end,
}

end

return task_list
