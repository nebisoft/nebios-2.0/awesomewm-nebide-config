local awful = require('awful')
local wibox = require('wibox')
local gears = require('gears')
local filesystem = gears.filesystem
local config_dir = filesystem.get_configuration_dir()
local dpi = require('beautiful').xresources.apply_dpi
local clickable_container = require('widget.clickable-container')
local widget_icon_dir = config_dir .. '/widget/search-apps/icons/'
local apps = require('configuration.apps')
local menubar = require("menubar")

local return_button = function()

	local widget = wibox.widget {
		{
			id = 'icon',
			image =  widget_icon_dir .. "slingscold.svg",
			widget = wibox.widget.imagebox,
			resize = true
		},
		layout = wibox.layout.align.horizontal
	}

	local widget_button = wibox.widget {
		{
			{
				widget,
				margins = dpi(16),
				widget = wibox.container.margin
			},
			shape = function(cr, width, height)
				gears.shape.rounded_rect(cr, width, height, dpi(12))
			end,
			widget = clickable_container
		},
		margins = dpi(4),
		widget = wibox.container.margin
	}
	
	local tooltip =  awful.tooltip {
		objects = {widget_button},
		text = "Launcher",
		mode = "outside",
		margin_leftright = dpi(10),
		margin_topbottom = dpi(6),
		preferred_alignments = {'middle'},
		preferred_positions = {'top', 'right', 'left'},
		align = 'top'
	}

	widget_button:buttons(
		gears.table.join(
			awful.button(
				{},
				1,
				nil,
				function()
					awful.spawn(apps.default.rofi_appmenu, false)
				end
			)
		)
	)

	return widget_button
end

return return_button
