local awful = require('awful')
local gears = require('gears')
local wibox = require('wibox')
local beautiful = require('beautiful')
local dpi = beautiful.xresources.apply_dpi
local config_dir = gears.filesystem.get_configuration_dir()
local widget_dir = config_dir .. 'widget/pinned-apps/'

local create_widgets = function(lay)
	local separator =  wibox.widget {
		orientation = 'horizontal',
		forced_height = dpi(1),
		forced_width = dpi(1),
		span_ratio = 0.25,
		widget = wibox.widget.separator
	}
	
	local appspath = widget_dir .. 'apps.txt' 

	widgetapp = wibox.widget {
		layout = lay
	}
	
	function string:endswith(suffix)
	    return self:sub(-#suffix) == suffix
	end
	
	local tooltip =  awful.tooltip {
		objects = {app_button},
		mode = "outside",
		margin_leftright = dpi(10),
		margin_topbottom = dpi(6),
		preferred_alignments = {'middle'},
		preferred_positions = {'top', 'right', 'left'},
		align = 'top'
	}
		
	awesome.connect_signal('pinned_apps::update', function() 
		local apps = {}
		file = io.open(appspath)
		appsline = file:lines()
		for i in appsline do
			if i:endswith(".desktop") then
				local deskfile = io.open(i, "r")
				if deskfile then
					obj = require("widget.pinned-apps.app")(i, tooltip)
					tooltip:add_to_object(obj)
			    		table.insert(apps, obj)
				end
			end
		end
		file:close()
		
		widgetapp:reset()
		widgetapp:add(table.unpack(apps))
		collectgarbage()
	end)
	
	awesome.emit_signal('pinned_apps::update')
	
	return widgetapp
end

return create_widgets
