local wibox = require('wibox')
local awful = require('awful')
local gears = require('gears')
local icons = require('theme.icons')
local ems = require('module.ems')
local clickable_container = require('widget.clickable-container')
local dpi = require('beautiful').xresources.apply_dpi
local utils = require("menubar.utils")
local config_dir = gears.filesystem.get_configuration_dir()
local widget_dir = config_dir .. 'widget/pinned-apps/'

local create_widget = function(desktopFile, tooltip)
	local app_widget =	wibox.widget {
		{
			image = utils.lookup_icon(utils.parse_desktop_file(desktopFile).Icon),
			resize = true,
			widget = wibox.widget.imagebox
		},
		layout = wibox.layout.align.horizontal
	}

	local app_button = wibox.widget {
		{
			{
				app_widget,
				margins = dpi(4),
				widget = wibox.container.margin
			},
			shape = function(cr, width, height)
				gears.shape.rounded_rect(cr, width, height, dpi(12))
			end,
			widget = clickable_container
		},
		margins = dpi(2),
		widget = wibox.container.margin
	}
	
	app_button:connect_signal("mouse::enter", function()
		tooltip.text = utils.parse_desktop_file(desktopFile).Name
	end)

	app_button:buttons(
		gears.table.join(
			awful.button(
				{},
				1,
				nil,
				function()
				awful.spawn(utils.parse_desktop_file(desktopFile).Exec:gsub("%%u", ""):gsub("%%U", ""), true)
				end
			),
			awful.button(
				{},
				3,
				nil,
				function()
				rclick = awful.menu({ { "Unpin", function() local cmd = 'sed -i "s+' .. desktopFile .. '++g" ' .. widget_dir .. 'apps.txt  && sed -i "/^$/d" ' .. widget_dir .. 'apps.txt && awesome-client \'awesome.emit_signal("pinned_apps::update")\''
				awful.spawn.with_shell(cmd) end, icons.close } })
                                              ems(rclick)
				end
			)
		)
	)

	return app_button
end

return create_widget
