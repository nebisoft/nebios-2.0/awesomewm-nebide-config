

local awful = require('awful')
local wibox = require('wibox')
local gears = require('gears')
local icons = require('theme.icons')
local filesystem = gears.filesystem
local config_dir = filesystem.get_configuration_dir()
local dpi = require('beautiful').xresources.apply_dpi
local clickable_container = require('widget.clickable-container')
local widget_icon_dir = config_dir .. '/widget/search-apps/icons/'
local apps = require('configuration.apps')
local menubar = require("menubar")

local return_button = function()

	local wtext = wibox.widget {
		markup = "   <span foreground='#ffffff'>Global Search</span>",
		halign = "center",
		valign = "center",
		font   = "Manrope Bold 9",
		widget = wibox.widget.textbox
	}
	
	local widget = wibox.widget {
		{
			{
				wibox.widget {
					markup = "⌕",
					halign = "center",
					valign = "center",
					font   = "Manrope Bold 16",
					widget = wibox.widget.textbox
				},
				wtext,
				layout = wibox.layout.align.horizontal
			},
			margins = {
				bottom = dpi(4),
				top = dpi(4),
				left = dpi(12),
				right = dpi(12)
			},
			widget = wibox.container.margin
		},
		bg = "#353535",
		shape = function(cr, width, height)
			gears.shape.partially_rounded_rect(
				cr, width, height, true, true, true, true, dpi(10)
			) 
		end,
		widget = wibox.container.background
	}

	local widget_button = wibox.widget {
		widget,
		margins = dpi(6),
		widget = wibox.container.margin
	}
	
	awesome.connect_signal("gs_set_wide", function(setbool) wtext.visible = setbool end)

	widget_button:buttons(
		gears.table.join(
			awful.button(
				{},
				1,
				nil,
				function()
					os.execute(apps.default.rofi_global .. " &")
				end
			)
		)
	)

	return widget_button
end

return return_button
