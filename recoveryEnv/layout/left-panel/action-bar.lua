local awful = require('awful')
local beautiful = require('beautiful')
local wibox = require('wibox')
local gears = require('gears')
local dpi = beautiful.xresources.apply_dpi
local icons = require('theme.icons')
local apps = require('configuration.apps')
local clickable_container = require('widget.clickable-container')
local task_list = require('widget.task-list')

return function(s, panel, action_bar_width)
	pinned = require("widget.pinned-apps")(s)

	local menu_icon = wibox.widget {
		{
			id = 'menu_btn',
			image = icons.menu,
			resize = true,
			widget = wibox.widget.imagebox
		},
		margins = dpi(10),
		widget = wibox.container.margin
	}
	
	local open_dashboard_button = wibox.widget {
		{
			menu_icon,
			widget = clickable_container
		},
		bg = beautiful.background .. '66',
		widget = wibox.container.background,
	}
	
local icons = require('theme.icons')
local wibox = require('wibox')
local beautiful = require('beautiful')
local dpi = beautiful.xresources.apply_dpi

local global_search_icon = wibox.widget {
	{
		id = 'menu_btn',
		image = icons.search,
		resize = true,
		widget = wibox.widget.imagebox
	},
	margins = dpi(10),
	widget = wibox.container.margin
}

local global_search_button = wibox.widget {
	{
		global_search_icon,
		widget = clickable_container
	},
	bg = beautiful.background .. '66',
	widget = wibox.container.background,
}

global_search_button:buttons(
	gears.table.join(
		awful.button(
			{},
			1,
			nil,
			function()
				os.execute(apps.default.rofi_global .. " &")
			end
		)
	)
)
	
	local ashley_icon = wibox.widget {
		{
			id = 'menu_btn',
			image = icons.ashley,
			resize = true,
			widget = wibox.widget.imagebox
		},
		margins = dpi(10),
		widget = wibox.container.margin
	}
	
	local ashley_button = wibox.widget {
		{
			ashley_icon,
			widget = clickable_container
		},
		bg = beautiful.background .. '66',
		widget = wibox.container.background,
	}
	
	ashley_button:buttons(
		gears.table.join(
			awful.button(
				{},
				1,
				nil,
				function()
					os.execute("/Applications/NebiAshley.napp &")
				end
			)
		)
	)

	global_search_button:buttons(
		gears.table.join(
			awful.button(
				{},
				1,
				nil,
				function()
					os.execute(apps.default.rofi_global .. " &")
				end
			)
		)
	)
	
	open_dashboard_button:buttons(
		gears.table.join(
			awful.button(
				{},
				1,
				nil,
				function()
					awful.screen.focused().control_center:toggle()
				end
			)
		)
	)
	
	local global_search_button_tooltip =  awful.tooltip {
		objects = {global_search_button},
		text = "Global Search",
		align = 'right'
	}
	
	local ashley_button_tooltip =  awful.tooltip {
		objects = {ashley_button},
		text = "NebiAshley Smart Assistant (Beta)",
		align = 'right'
	}
	
	local open_dashboard_button_tooltip =  awful.tooltip {
		objects = {open_dashboard_button},
		text = "Quick Settings",
		align = 'right'
	}
	
	panel:connect_signal(
		'update_rot',
		function(rot)
			print(rot)
			if rot == "left" then
				-- Left
			elseif rot == "bottom" then
				-- Bottom
			else
				os.execute('zenity --info --text="Only \\"left\\" and \\"bottom\\" accepted." &')
			end
		end
	)

	panel:connect_signal(
		'opened',
		function()
			menu_icon.menu_btn:set_image(gears.surface(icons.close_small))
		end
	)

	panel:connect_signal(
		'closed',
		function()
			menu_icon.menu_btn:set_image(gears.surface(icons.menu))
		end
	)
	
	local add_button 		= require('widget.open-default-app')(s)
	
	local separator =  wibox.widget {
		orientation = 'horizontal',
		forced_height = dpi(1),
		forced_width = dpi(1),
		span_ratio = 0.25,
		widget = wibox.widget.separator
	}
	
	widget = wibox.widget {
		id = 'action_bar',
		layout = wibox.layout.align.vertical,
		margins = dpi(5),
		{
			require('widget.search-apps')(),
			global_search_button,
			ashley_button,
			separator,
			pinned,
			separator,
			task_list(s),
			add_button,
			layout = wibox.layout.fixed.vertical,
		},
		nil,
		{
			require("widget.xdg-folders")(),
			separator,
			open_dashboard_button,
			layout = wibox.layout.fixed.vertical,
		}
	}
	
	s:connect_signal(
			'update_rot',
			function(arg1, rot)
				if rot == "left" then
					separator =  wibox.widget {
						orientation = 'horizontal',
						forced_height = dpi(1),
						forced_width = dpi(1),
						span_ratio = 0.25,
						widget = wibox.widget.separator
					}
					widget.layout = wibox.layout.align.vertical
					widget[1] = {
							require('widget.search-apps')(),
							global_search_button,
							ashley_button,
							separator,
							pinned,
							separator,
							task_list(s),
							add_button,
							layout = wibox.layout.fixed.vertical,
						}
					widget[2] = nil
					widget[3] = {
							require("widget.xdg-folders")(),
							separator,
							open_dashboard_button,
							layout = wibox.layout.fixed.vertical,
						}
				elseif rot == "bottom" then
					separator =  wibox.widget {
						orientation = 'vertical',
						forced_height = dpi(1),
						forced_width = dpi(1),
						span_ratio = 0.25,
						widget = wibox.widget.separator
					}
					widget.layout = wibox.layout.align.horizontal
					widget[1] = {
							require('widget.search-apps')(),
							global_search_button,
							ashley_button,
							separator,
							pinned,
							separator,
							task_list(s),
							add_button,
							layout = wibox.layout.fixed.horizontal,
						}
					widget[2] = nil
					widget[3] = {
							require("widget.xdg-folders")(),
							separator,
							open_dashboard_button,
							layout = wibox.layout.fixed.horizontal,
						}
				end
			end
		)

	return widget
end
