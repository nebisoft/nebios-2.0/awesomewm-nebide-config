local awful = require('awful')
local top_panel = require('layout.top-panel')
local right_panel = require('layout.right-panel')
local bottom_panel = require('layout.bottom-panel')
local control_center = require('layout.control-center')
-- local gesturepad = require('layout.gesturepad')
local beautiful = require('beautiful')
local dpi = beautiful.xresources.apply_dpi
local action_bar_width = dpi(10 + beautiful.action_bar_icon_size)

awesome.connect_signal("screen_dpi_changed", function(screen)
	require("xresources").set_dpi(screen.dpi)
end)

m_bottom_panel = bottom_panel(screen.primary)

function re_init_utility_panel()
    awesome.emit_signal("bottom_panel::update_screen", screen.primary)
    os.execute("killall nebide-we && nebide-we &")
    os.execute("nebide-shellconf &")
end

-- Connect the function to multiple signals on the screen
screen.connect_signal("primary_changed", re_init_utility_panel)
screen.connect_signal("add", re_init_utility_panel)
screen.connect_signal("remove", re_init_utility_panel)
screen.connect_signal("property::geometry", re_init_utility_panel)

-- Create a wibox panel for each screen and add it
screen.connect_signal(
	'request::desktop_decoration',
	function(s)
		s.top_panel = top_panel(s)
		s.right_panel = right_panel(s)
		s.control_center = control_center(s)
		s.control_center_show_again = false
		if screen.primary == s then
			-- s.gesturepad = gesturepad(s)
			s.bottom_panel = m_bottom_panel
			awesome.emit_signal("bottom_panel::update_screen", screen.primary)
		end
	end
)


-- Hide bars when app go fullscreen
function update_bars_visibility()
	for s in screen do
		if s.selected_tag then
			local fullscreen = s.selected_tag.fullscreen_mode
			-- Order matter here for shadow
			s.top_panel.visible = not fullscreen
			if screen.primary == s then
				if s.bottom_panel then
					s.bottom_panel[1].visible = not fullscreen
					s.bottom_panel[2].visible = not fullscreen
					s.bottom_panel[3].visible = not fullscreen
				end
			end
			if s.control_center then
				if fullscreen and s.control_center.visible then
					s.control_center:toggle()
					s.control_center_show_again = true
				elseif not fullscreen and not s.control_center.visible and s.control_center_show_again then
					s.control_center:toggle()
					s.control_center_show_again = false
				end
			end
		end
	end
end

tag.connect_signal(
	'property::selected',
	function(t)
		update_bars_visibility()
	end
)

client.connect_signal(
	'property::fullscreen',
	function(c)
		if c.type == "desktop" then else
			if c.first_tag then
				c.first_tag.fullscreen_mode = c.fullscreen
			end
			update_bars_visibility()
		end
	end
)

client.connect_signal(
	'unmanage',
	function(c)
		if c.fullscreen then
			c.screen.selected_tag.fullscreen_mode = false
			update_bars_visibility()
		end
	end
)
