local awful = require('awful')
local beautiful = require('beautiful')
local wibox = require('wibox')
local gears = require('gears')
local icons = require('theme.icons')
local dpi = beautiful.xresources.apply_dpi
local tag_list = require('widget.tag-list')
local clickable_container = require('widget.clickable-container')
awesome.register_xproperty("WM_NAME", "string")
local latest_dock_setup = "nil"
local panel_shown = true

separator =  wibox.widget {
		orientation = 'vertical',
		forced_height = dpi(1),
		forced_width = dpi(1),
		span_ratio = 0.25,
		widget = wibox.widget.separator
	}

local bottom_panel = function(s)
	scr = s
	local panel_shown = true
	
	launchers_panel = awful.popup
	{
		ontop = true,
		screen = scr,
		type = 'dock',
		maximum_width = 1,
		border_width = dpi(1),
		border_color = "#00000000",
		bg = beautiful.background,
		visible = false,
		shape = function(cr, width, height)
			gears.shape.partially_rounded_rect(
				cr, width, height, true, true, true, true, dpi(14)
			) 
		end,
		widget = {}
	}
	
	dock_panel = awful.popup
	{
		ontop = true,
		screen = scr,
		type = 'dock',
		maximum_width = 1,
		bg = "#00000000",
		visible = false,
		shape = function(cr, width, height)
			gears.shape.partially_rounded_rect(
				cr, width, height, true, true, true, true, dpi(14)
			) 
		end,
		widget = {}
	}
		
	util_panel = awful.popup
	{
		ontop = true,
		screen = scr,
		type = 'dock',
		maximum_width = 1,
		bg = beautiful.background,
		border_width = dpi(1),
		border_color = "#00000000",
		visible = false,
		shape = function(cr, width, height)
			gears.shape.partially_rounded_rect(
				cr, width, height, true, true, true, true, dpi(14)
			) 
		end,
		widget = {}
	}
	
	scr.searchapps  		= require('widget.search-apps')

	launchers_panel : setup {
		layout = wibox.layout.fixed.horizontal,
		scr.searchapps()
	}
	
	function setup_dock(lay)
		scr.pinnedapps    	= nil
		scr.tasklist   		= nil
		scr.bookmarks     	= nil
		scr.trash       		= nil
		scr.pinnedapps    	= require('widget.pinned-apps')(lay)
		scr.tasklist   		= require('widget.task-list')(s, lay)
		scr.bookmarks     	= require('widget.xdg-bookmarks')(lay)
		scr.trash       		= require('widget.xdg-trash')

		dock_panel : setup {
			layout = lay,
			spacing = dpi(5),
			wibox.container.margin (
				wibox.container.background (
					wibox.container.margin (
						scr.pinnedapps,
						dpi(2),
						dpi(2),
						dpi(2),
						dpi(2)
					),
					beautiful.background,
					function(cr, width, height)
						gears.shape.partially_rounded_rect(
							cr, width, height, true, true, true, true, dpi(15)
						) 
					end
				),
				dpi(1),
				dpi(1),
				dpi(1),
				dpi(1)
			),
			wibox.container.margin (
				wibox.container.background (
					wibox.container.margin(
						scr.tasklist,
						dpi(2),
						dpi(2),
						dpi(2),
						dpi(2)
					),
					beautiful.background,
					function(cr, width, height)
						gears.shape.partially_rounded_rect(
							cr, width, height, true, true, true, true, dpi(15)
						) 
					end
				),
				dpi(1),
				dpi(1),
				dpi(1),
				dpi(1)
			),
			wibox.container.margin (
				wibox.container.background (
					wibox.container.margin(
						scr.bookmarks,
						dpi(2),
						dpi(2),
						dpi(2),
						dpi(2)
					),
					beautiful.background,
					function(cr, width, height)
						gears.shape.partially_rounded_rect(
							cr, width, height, true, true, true, true, dpi(15)
						) 
					end
				),
				dpi(1),
				dpi(1),
				dpi(1),
				dpi(1)
			),
			wibox.container.margin (
				wibox.container.background (
					scr.trash(),
					beautiful.background,
					function(cr, width, height)
						gears.shape.partially_rounded_rect(
							cr, width, height, true, true, true, true, dpi(15)
						) 
					end
				),
				dpi(1),
				dpi(1),
				dpi(1),
				dpi(1)
			)
		}
	end
	
	--scr.multitasking		= require('widget.app-view')
	scr.controltoggle		= require('widget.control-center-toggle')
	
	util_panel : setup {
		layout = wibox.layout.fixed.horizontal,
		--scr.multitasking,
		scr.controltoggle()
	}
	
	launchers_panel.visible = true
	dock_panel.visible = true
	util_panel.visible = true
	
	launchers_panel:set_xproperty("WM_NAME", "NebiDE_bottom_launchers")
	dock_panel:set_xproperty("WM_NAME", "NebiDE_bottom_dock")
	util_panel:set_xproperty("WM_NAME", "NebiDE_bottom_util")
	
	awesome.connect_signal("bottom_panel::change_hide_policy", function(pol)
		beautiful.bottom_panel_hide_policy = pol
		if pol == "smart" then
			dock_panel:struts {
				left = 0,
				right = 0,
				top = 0,
				bottom = 0
			}
			
			if string.sub(beautiful.bottom_panel_placement, 1, 3) == "btm" then
				scr.panel_hotedge = wibox {
					screen = s,
					ontop = true,
					bg = "#00000000",
					opacity = 0,
					stretch = true,
					visible = true,
					width = scr.geometry.width,
					height = dpi(1),
					x = scr.geometry.x,
					y = scr.geometry.height - dpi(1)
				}
			elseif string.sub(beautiful.bottom_panel_placement, 1, 3) == "lft" then
				scr.panel_hotedge = wibox {
					screen = s,
					ontop = true,
					bg = "#00000000",
					opacity = 0,
					stretch = true,
					visible = true,
					width = dpi(1),
					height = scr.geometry.height,
					x = scr.geometry.x,
					y = scr.geometry.y
				}
			elseif string.sub(beautiful.bottom_panel_placement, 1, 3) == "rig" then
				scr.panel_hotedge = wibox {
					screen = s,
					ontop = true,
					bg = "#00000000",
					opacity = 0,
					stretch = true,
					visible = true,
					width = dpi(1),
					height = scr.geometry.height,
					x = scr.geometry.width - dpi(1),
					y = scr.geometry.y
				}
			end
			
			scr.panel_hotedge:connect_signal("mouse::enter", function()
				if panel_shown == false then
					scr.panel_hotedge_timer = gears.timer.start_new (1, function()
						client.focus = nil
						scr.panel_hotedge_timer:stop()
						awesome.emit_signal("bottom_panel::is_shown", true)
					end)
				end
		    	end)
		end
		if pol == "nil" then
			launchers_panel.visible = true
			dock_panel.visible = true
			util_panel.visible = true
			scr.panel_hotedge = nil
			scr.panel_hotedge_timer = nil
			if string.sub(beautiful.bottom_panel_placement, 1, 3) == "btm" then
				dock_panel:struts {
					left = 0,
					right = 0,
					top = 0,
					bottom = dpi(40 + beautiful.action_bar_icon_size)
				}
			elseif string.sub(beautiful.bottom_panel_placement, 1, 3) == "lft" then
				dock_panel:struts {
					left = dpi(40 + beautiful.action_bar_icon_size),
					right = 0,
					top = 0,
					bottom = 0
				}
			elseif string.sub(beautiful.bottom_panel_placement, 1, 3) == "rig" then
				dock_panel:struts {
					left = 0,
					right = dpi(40 + beautiful.action_bar_icon_size),
					top = 0,
					bottom = 0
				}
			end
		    	awesome.emit_signal("bottom_panel::is_shown", true)
		end
		collectgarbage()
	end)
	
	awesome.connect_signal("bottom_panel::is_shown", function(is_shown)
		if not panel_shown == is_shown then
			panel_shown = is_shown
			
			if is_shown == true then
				awesome.emit_signal("bottom_panel::update_sizes", "none", beautiful.action_bar_icon_size)
				launchers_panel.visible = is_shown
				dock_panel.visible = is_shown
				util_panel.visible = is_shown
			else
				awesome.emit_signal("bottom_panel::update_sizes", "none", 3)
			end
			
			gears.timer.start_new(0.75, function()
				launchers_panel.visible = is_shown
				dock_panel.visible = is_shown
				util_panel.visible = is_shown
			end)
		end
		collectgarbage()
	end)
	
	awesome.connect_signal("bottom_panel::update_screen", function(new_scr)
		scr = new_scr
		launchers_panel.screen = new_scr
		dock_panel.screen = new_scr
		util_panel.screen = new_scr
		awesome.emit_signal("bottom_panel::update_sizes", "none")
	end)

	    awesome.emit_signal("bottom_panel::change_hide_policy", beautiful.bottom_panel_hide_policy)
	    collectgarbage()
	end)

	local function update_panel_sizes(laypos, size)
    size = size or beautiful.action_bar_icon_size
    local margin_num = panel_shown and 10 or 0

    if laypos and laypos ~= beautiful.bottom_panel_placement then
        if laypos ~= "none" then
            beautiful.bottom_panel_placement = laypos
            awesome.emit_signal("bottom_panel::update_layout", margin_num)
        end
    end

    local launchers_panel_max_size = dpi(margin_num * 2 + size)
    local dock_panel_max_size = string.sub(beautiful.bottom_panel_placement, 1, 3) == "btm" and scr.geometry.width - (margin_num * 2 + size * 2) or scr.geometry.height - (margin_num * 2 + size * 2 + 36)

    launchers_panel.maximum_height = launchers_panel_max_size
    dock_panel.maximum_height = dock_panel_max_size
    util_panel.maximum_height = launchers_panel_max_size
    launchers_panel.maximum_width = launchers_panel_max_size
    dock_panel.maximum_width = launchers_panel_max_size
    util_panel.maximum_width = launchers_panel_max_size

    awesome.connect_signal("bottom_panel::update_sizes", function(laypos, size)
		size = size or beautiful.action_bar_icon_size
		if panel_shown == true then
			margin_num = 10
		else
			margin_num = 0
		end
		
		if laypos == beautiful.bottom_panel_placement then else
			if laypos == "none" then else
				beautiful.bottom_panel_placement = laypos
			end
			
			if string.sub(beautiful.bottom_panel_placement, 1, 3) == "btm" then
				launchers_panel.maximum_height = dpi(margin_num + margin_num + size)
				dock_panel.maximum_height = dpi(margin_num + margin_num + size)
				util_panel.maximum_height = dpi(margin_num + margin_num + size)
				launchers_panel.maximum_width = dpi(margin_num + margin_num + size)
				dock_panel.maximum_width = dpi(scr.geometry.width - ((margin_num + margin_num + size) * 2))
				util_panel.maximum_width = dpi(margin_num + margin_num + size)
			else 
				launchers_panel.maximum_width = dpi(margin_num + margin_num + size)
				dock_panel.maximum_width = dpi(margin_num + margin_num + size)
				util_panel.maximum_width = dpi(margin_num + margin_num + size)
				launchers_panel.maximum_height = dpi(margin_num + margin_num + size)
				dock_panel.maximum_height = dpi(scr.geometry.height - ((margin_num + margin_num + size) * 2) - 36)
				util_panel.maximum_height = dpi(margin_num + margin_num + size)
			end
			
			if laypos == "none" then else
				awesome.emit_signal("bottom_panel::update_layout", margin_num)
			end
		end
		awesome.emit_signal("bottom_panel::change_hide_policy", beautiful.bottom_panel_hide_policy)
		collectgarbage()
	end)
	
	awesome.connect_signal("bottom_panel::update_layout", function(margin_px)
		topbar_size = scr.top_panel.height
		if latest_dock_setup == string.sub(beautiful.bottom_panel_placement, 1, 3) then else
			if string.sub(beautiful.bottom_panel_placement, 1, 3) == "btm" then
				setup_dock(wibox.layout.fixed.horizontal)
			else
				setup_dock(wibox.layout.fixed.vertical)
			end
			latest_dock_setup = string.sub(beautiful.bottom_panel_placement, 1, 3)	
		end

		if beautiful.bottom_panel_placement == "lft_top" then
			launchers_panel.placement = function(c)
			    return awful.placement.top_left(c, { margins = { top = dpi(topbar_size + margin_px), left = dpi(margin_px), right = dpi(margin_px), bottom = dpi(margin_px) } })
			end
			dock_panel.placement = function(c)
			    return awful.placement.top_left(c, { margins = { top = dpi((margin_px * 2) + beautiful.action_bar_icon_size + topbar_size + (margin_px * 2)), left = dpi(margin_px), right = dpi(margin_px), bottom = dpi((margin_px * 2) + beautiful.action_bar_icon_size + (margin_px * 2)) } })
			end
			util_panel.placement = function(c)
			    return awful.placement.bottom_left(c, { margins = dpi(margin_px) })
			end
		end

		if beautiful.bottom_panel_placement == "lft_mid" then
			launchers_panel.placement = function(c)
			    return awful.placement.top_left(c, { margins = { top = dpi(topbar_size + margin_px), left = dpi(margin_px), right = dpi(margin_px), bottom = dpi(margin_px) } })
			end
			dock_panel.placement = function(c)
			    return awful.placement.left(c, { margins = { top = dpi((margin_px * 2) + beautiful.action_bar_icon_size + topbar_size + (margin_px * 2)), left = dpi(margin_px), right = dpi(margin_px), bottom = dpi((margin_px * 2) + beautiful.action_bar_icon_size + (margin_px * 2)) } })
			end
			util_panel.placement = function(c)
			    return awful.placement.bottom_left(c, { margins = dpi(margin_px) })
			end
		end

		if beautiful.bottom_panel_placement == "lft_btm" then
			launchers_panel.placement = function(c)
			    return awful.placement.top_left(c, { margins = { top = dpi(topbar_size + margin_px), left = dpi(margin_px), right = dpi(margin_px), bottom = dpi(margin_px) } })
			end
			dock_panel.placement = function(c)
			    return awful.placement.bottom_left(c, { margins = { top = dpi((margin_px * 2) + beautiful.action_bar_icon_size + topbar_size + (margin_px * 2)), left = dpi(margin_px), right = dpi(margin_px), bottom = dpi((margin_px * 2) + beautiful.action_bar_icon_size + (margin_px * 2)) } })
			end
			util_panel.placement = function(c)
			    return awful.placement.bottom_left(c, { margins = dpi(margin_px) })
			end
		end

		if beautiful.bottom_panel_placement == "btm_lft" then
			launchers_panel.placement = function(c)
			    return awful.placement.bottom_left(c, { margins =  dpi(margin_px) })
			end
			dock_panel.placement = function(c)
			    return awful.placement.bottom_left(c, { margins = { left = dpi((margin_px * 2) + beautiful.action_bar_icon_size + 0 + (margin_px * 2)), top = dpi(margin_px), bottom = dpi(margin_px), right = dpi((margin_px * 2) + beautiful.action_bar_icon_size + 0 + (margin_px * 2)) } })
			end
			util_panel.placement = function(c)
			    return awful.placement.bottom_right(c, { margins = dpi(margin_px) })
			end
		end

		if beautiful.bottom_panel_placement == "btm_mid" then
			launchers_panel.placement = function(c)
			    return awful.placement.bottom_left(c, { margins =  dpi(margin_px) })
			end
			dock_panel.placement = function(c)
			    return awful.placement.bottom(c, { margins = { left = dpi((margin_px * 2) + beautiful.action_bar_icon_size + 0 + (margin_px * 2)), top = dpi(margin_px), bottom = dpi(margin_px), right = dpi((margin_px * 2) + beautiful.action_bar_icon_size + 0 + (margin_px * 2)) } })
			end
			util_panel.placement = function(c)
			    return awful.placement.bottom_right(c, { margins = dpi(margin_px) })
			end
		end 

		if beautiful.bottom_panel_placement == "btm_rig" then
			launchers_panel.placement = function(c)
			    return awful.placement.bottom_left(c, { margins =  dpi(margin_px) })
			end
			dock_panel.placement = function(c)
			    return awful.placement.bottom_right(c, { margins = { left = dpi((margin_px * 2) + beautiful.action_bar_icon_size + 0 + (margin_px * 2)), top = dpi(margin_px), bottom = dpi(margin_px), right = dpi((margin_px * 2) + beautiful.action_bar_icon_size + 0 + (margin_px * 2)) } })
			end
			util_panel.placement = function(c)
			    return awful.placement.bottom_right(c, { margins = dpi(margin_px) })
			end
		end 

		if beautiful.bottom_panel_placement == "rig_top" then
			launchers_panel.placement = function(c)
			    return awful.placement.top_right(c, { margins = { top = dpi(topbar_size + margin_px), left = dpi(margin_px), right = dpi(margin_px), bottom = dpi(margin_px) } })
			end
			dock_panel.placement = function(c)
			    return awful.placement.top_right(c, { margins = { top = dpi((margin_px * 2) + beautiful.action_bar_icon_size + topbar_size + (margin_px * 2)), left = dpi(margin_px), right = dpi(margin_px), bottom = dpi((margin_px * 2) + beautiful.action_bar_icon_size + (margin_px * 2)) } })
			end
			util_panel.placement = function(c)
			    return awful.placement.bottom_right(c, { margins = dpi(margin_px) })
			end
		end 

		if beautiful.bottom_panel_placement == "rig_mid" then
			launchers_panel.placement = function(c)
			    return awful.placement.top_right(c, { margins = { top = dpi(topbar_size + margin_px), left = dpi(margin_px), right = dpi(margin_px), bottom = dpi(margin_px) } })
			end
			dock_panel.placement = function(c)
			    return awful.placement.right(c, { margins = { top = dpi((margin_px * 2) + beautiful.action_bar_icon_size + topbar_size + (margin_px * 2)), left = dpi(margin_px), right = dpi(margin_px), bottom = dpi((margin_px * 2) + beautiful.action_bar_icon_size + (margin_px * 2)) } })
			end
			util_panel.placement = function(c)
			    return awful.placement.bottom_right(c, { margins = dpi(margin_px) })
			end
		end 

		if beautiful.bottom_panel_placement == "rig_btm" then
			launchers_panel.placement = function(c)
			    return awful.placement.top_right(c, { margins = { top = dpi(topbar_size + margin_px), left = dpi(margin_px), right = dpi(margin_px), bottom = dpi(margin_px) } })
			end
			dock_panel.placement = function(c)
			    return awful.placement.bottom_right(c, { margins = { top = dpi((margin_px * 2) + beautiful.action_bar_icon_size + topbar_size + (margin_px * 2)), left = dpi(margin_px), right = dpi(margin_px), bottom = dpi((margin_px * 2) + beautiful.action_bar_icon_size + (margin_px * 2)) } })
			end
			util_panel.placement = function(c)
			    return awful.placement.bottom_right(c, { margins = dpi(margin_px) })
			end
		end
		collectgarbage()
	end
	)

	
	return { launchers_panel, dock_panel, util_panel }
end

return bottom_panel
