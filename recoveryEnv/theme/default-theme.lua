local gears = require('gears')
local beautiful = require('beautiful')
local osext = require('library.osext')

local filesystem = gears.filesystem
local dpi = beautiful.xresources.apply_dpi
local gtk_variable = beautiful.gtk.get_theme_variables

local theme_dir = filesystem.get_configuration_dir() .. '/theme'
local titlebar_theme = 'default'
local titlebar_icon_path = theme_dir .. '/icons/titlebar/'
local tip = titlebar_icon_path

-- Create theme table
local theme = {}

-- Font
theme.font = 'Manrope Medium 10'
theme.font_bold = 'Manrope Bold 10'

-- Menu icon theme
theme.icon_theme = osext.capture([[
	dconf read /org/mate/desktop/interface/icon-theme | sed "s+'++g"
]])

local awesome_overrides = function(theme)

	theme.dir = theme_dir
	theme.icons = theme_dir .. '/icons/'

	-- Default wallpaper path
	theme.wallpaper = theme.dir .. '/wallpapers/morning-wallpaper.jpg'

	-- Default font
	theme.font = 'Manrope Medium 10'
	
	-- Icon size on actionbar
	theme.action_bar_icon_size = 32
	
	theme.bottom_panel_hide_policy = "smart" 
	
	-- Foreground
	theme.fg_normal = "#f5f5f5"
	theme.fg_focus = "#ffffff"
	theme.fg_urgent = '#c5c5c5'

	theme.bg_normal = gtk_variable().theme_bg_color
	theme.bg_focus = gtk_variable().theme_selected_bg_color
	theme.bg_urgent = '#3F3F3F'

	-- System tray
	theme.bg_systray = theme.background
	theme.systray_icon_spacing = dpi(16)

	-- Titlebar
	theme.titlebar_size = dpi(40)
	theme.titlebar_resize_border_size = dpi(6)
	theme.titlebar_position = "top" 
	theme.titlebar_layout = "c:it:m" 
	theme.titlebar_theme = "default"
	theme.titlebar_icon_path = tip
	theme.titlebar_bg_focus = gtk_variable().base_color
	theme.titlebar_bg_normal = gtk_variable().base_color
	theme.titlebar_fg_focus = gtk_variable().fg_color
	theme.titlebar_fg_normal = gtk_variable().fg_color

	-- UI Groups
	theme.groups_title_bg = '#ffffff' .. '15'
	theme.groups_bg = '#ffffff' .. '10'
	theme.groups_radius = dpi(12)

	-- UI events
	theme.leave_event = transparent
	theme.enter_event = '#ffffff' .. '10'
	theme.press_event = '#ffffff' .. '15'
	theme.release_event = '#ffffff' .. '10'

	-- Client Decorations

	-- Borders
	theme.border_focus = ""
	theme.border_normal = ""
	theme.border_marked = ""
	theme.border_width = 0
	theme.border_radius = dpi(15)

	-- Decorations
	theme.useless_gap = dpi(4)
	theme.client_shape_rectangle = gears.shape.rectangle
	theme.client_shape_rounded = function(cr, width, height)
		gears.shape.rounded_rect(cr, width, height, dpi(12))
	end

	-- Menu
	theme.menu_font = 'Manrope Medium 10'
	theme.menu_submenu = '>' -- ➤

	theme.menu_height = dpi(24)
	theme.menu_width = dpi(240)
	theme.menu_border_width = dpi(8)
	theme.menu_shape = function(cr, w, h)
		gears.shape.rounded_rect(cr, w, h, dpi(12))
	end

	theme.menu_bg_normal =  gtk_variable().base_color:sub(1,7) .. "EE"
	theme.menu_fg_normal = gtk_variable().menubar_fg_color
	theme.menu_bg_focus = gtk_variable().selected_bg_color
	theme.menu_fg_focus = gtk_variable().selected_fg_color
	theme.menu_border_color = gtk_variable().bg_color:sub(1,7) .. "01"

	-- Tooltips

	theme.tooltip_bg = theme.background
	theme.tooltip_border_color = theme.transparent
	theme.tooltip_border_width = dpi(1)
	theme.tooltip_gaps = dpi(10)
	theme.tooltip_shape = function(cr, w, h)
		gears.shape.rounded_rect(cr, w, h, dpi(15))
	end

	-- Separators
	theme.separator_color = '#f5f5f544'

	-- Layoutbox icons
	theme.layout_max = theme.icons .. 'layouts/max.svg'
	theme.layout_tile = theme.icons .. 'layouts/tile.svg'
	theme.layout_dwindle = theme.icons .. 'layouts/dwindle.svg'
	theme.layout_floating = theme.icons .. 'layouts/floating.svg'
	
	-- Taglist
	theme.taglist_bg_empty = theme.background .. '99'
	theme.taglist_bg_occupied =  '#ffffff' .. '1A'
	theme.taglist_bg_urgent = '#E91E63' .. '99'
	theme.taglist_bg_focus = theme.background
	theme.taglist_spacing = dpi(0)

	-- Tasklist
	theme.tasklist_font = 'Manrope Medium 10'
	theme.tasklist_bg_normal = theme.background .. '99'
	theme.tasklist_bg_focus = '#ffffff' .. '15'
	theme.tasklist_bg_urgent = '#E91E63' .. '99'
	theme.tasklist_fg_focus = '#DDDDDD'
	theme.tasklist_fg_urgent = '#ffffff'
	theme.tasklist_fg_normal = '#AAAAAA'

	-- Notification
	theme.notification_position = 'bottom_right'
	theme.notification_bg = theme.transparent
	theme.notification_margin = 0
	theme.notification_border_width = dpi(1)
	theme.notification_border_color = "#00000000"
	theme.notification_spacing = dpi(10)
	theme.notification_icon_resize_strategy = 'center'
	theme.notification_icon_size = dpi(48)
	
	-- Client Snap Theme
	theme.snap_bg = theme.background
	theme.snap_shape = gears.shape.rectangle
	theme.snap_border_width = dpi(15)

	-- Hotkey popup
	theme.hotkeys_font = 'Manrope Bold'
	theme.hotkeys_description_font   = 'Manrope Medium Regular'
	theme.hotkeys_bg = theme.background
	theme.hotkeys_group_margin = dpi(20)
end

awesome.connect_signal('titlebar::update_icon_theme_vars', function()
	-- Close Button
	beautiful.titlebar_close_button_normal = tip .. beautiful.titlebar_theme .. '/' .. 'close_normal.svg'
	beautiful.titlebar_close_button_focus  = tip .. beautiful.titlebar_theme .. '/' .. 'close_focus.svg'

	-- Minimize Button
	beautiful.titlebar_minimize_button_normal = tip .. beautiful.titlebar_theme .. '/' .. 'minimize_normal.svg'
	beautiful.titlebar_minimize_button_focus  = tip .. beautiful.titlebar_theme .. '/' .. 'minimize_focus.svg'

	-- Ontop Button
	beautiful.titlebar_ontop_button_normal_inactive = tip .. beautiful.titlebar_theme .. '/' .. 'ontop_normal_inactive.svg'
	beautiful.titlebar_ontop_button_focus_inactive  = tip .. beautiful.titlebar_theme .. '/' .. 'ontop_focus_inactive.svg'
	beautiful.titlebar_ontop_button_normal_active = tip .. beautiful.titlebar_theme .. '/' .. 'ontop_normal_active.svg'
	beautiful.titlebar_ontop_button_focus_active  = tip .. beautiful.titlebar_theme .. '/' .. 'ontop_focus_active.svg'

	-- Sticky Button
	beautiful.titlebar_sticky_button_normal_inactive = tip .. beautiful.titlebar_theme .. '/' .. 'sticky_normal_inactive.svg'
	beautiful.titlebar_sticky_button_focus_inactive  = tip .. beautiful.titlebar_theme .. '/' .. 'sticky_focus_inactive.svg'
	beautiful.titlebar_sticky_button_normal_active = tip .. beautiful.titlebar_theme .. '/' .. 'sticky_normal_active.svg'
	beautiful.titlebar_sticky_button_focus_active  = tip .. beautiful.titlebar_theme .. '/' .. 'sticky_focus_active.svg'

	-- Floating Button
	beautiful.titlebar_floating_button_normal_inactive = tip .. beautiful.titlebar_theme .. '/' .. 'floating_normal_inactive.svg'
	beautiful.titlebar_floating_button_focus_inactive  = tip .. beautiful.titlebar_theme .. '/' .. 'floating_focus_inactive.svg'
	beautiful.titlebar_floating_button_normal_active = tip .. beautiful.titlebar_theme .. '/' .. 'floating_normal_active.svg'
	beautiful.titlebar_floating_button_focus_active  = tip .. beautiful.titlebar_theme .. '/' .. 'floating_focus_active.svg'

	-- Maximized Button
	beautiful.titlebar_maximized_button_normal_inactive = tip .. beautiful.titlebar_theme .. '/' .. 'maximized_normal_inactive.svg'
	beautiful.titlebar_maximized_button_focus_inactive  = tip .. beautiful.titlebar_theme .. '/' .. 'maximized_focus_inactive.svg'
	beautiful.titlebar_maximized_button_normal_active = tip .. beautiful.titlebar_theme .. '/' .. 'maximized_normal_active.svg'
	beautiful.titlebar_maximized_button_focus_active  = tip .. beautiful.titlebar_theme .. '/' .. 'maximized_focus_active.svg'

	-- Hovered Close Button
	beautiful.titlebar_close_button_normal_hover = tip .. beautiful.titlebar_theme .. '/' .. 'close_normal_hover.svg'
	beautiful.titlebar_close_button_focus_hover  = tip .. beautiful.titlebar_theme .. '/' .. 'close_focus_hover.svg'

	-- Hovered Minimize Buttin
	beautiful.titlebar_minimize_button_normal_hover = tip .. beautiful.titlebar_theme .. '/' .. 'minimize_normal_hover.svg'
	beautiful.titlebar_minimize_button_focus_hover  = tip .. beautiful.titlebar_theme .. '/' .. 'minimize_focus_hover.svg'

	-- Hovered Ontop Button
	beautiful.titlebar_ontop_button_normal_inactive_hover = tip .. beautiful.titlebar_theme .. '/' .. 'ontop_normal_inactive_hover.svg'
	beautiful.titlebar_ontop_button_focus_inactive_hover  = tip .. beautiful.titlebar_theme .. '/' .. 'ontop_focus_inactive_hover.svg'
	beautiful.titlebar_ontop_button_normal_active_hover = tip .. beautiful.titlebar_theme .. '/' .. 'ontop_normal_active_hover.svg'
	beautiful.titlebar_ontop_button_focus_active_hover  = tip .. beautiful.titlebar_theme .. '/' .. 'ontop_focus_active_hover.svg'

	-- Hovered Sticky Button
	beautiful.titlebar_sticky_button_normal_inactive_hover = tip .. beautiful.titlebar_theme .. '/' .. 'sticky_normal_inactive_hover.svg'
	beautiful.titlebar_sticky_button_focus_inactive_hover  = tip .. beautiful.titlebar_theme .. '/' .. 'sticky_focus_inactive_hover.svg'
	beautiful.titlebar_sticky_button_normal_active_hover = tip .. beautiful.titlebar_theme .. '/' .. 'sticky_normal_active_hover.svg'
	beautiful.titlebar_sticky_button_focus_active_hover  = tip .. beautiful.titlebar_theme .. '/' .. 'sticky_focus_active_hover.svg'

	-- Hovered Floating Button
	beautiful.titlebar_floating_button_normal_inactive_hover = tip .. beautiful.titlebar_theme .. '/' .. 'floating_normal_inactive_hover.svg'
	beautiful.titlebar_floating_button_focus_inactive_hover  = tip .. beautiful.titlebar_theme .. '/' .. 'floating_focus_inactive_hover.svg'
	beautiful.titlebar_floating_button_normal_active_hover = tip .. beautiful.titlebar_theme .. '/' .. 'floating_normal_active_hover.svg'
	beautiful.titlebar_floating_button_focus_active_hover  = tip .. beautiful.titlebar_theme .. '/' .. 'floating_focus_active_hover.svg'

	-- Hovered Maximized Button
	beautiful.titlebar_maximized_button_normal_inactive_hover = tip .. beautiful.titlebar_theme .. '/' .. 'maximized_normal_inactive_hover.svg'
	beautiful.titlebar_maximized_button_focus_inactive_hover  = tip .. beautiful.titlebar_theme .. '/' .. 'maximized_focus_inactive_hover.svg'
	beautiful.titlebar_maximized_button_normal_active_hover = tip .. beautiful.titlebar_theme .. '/' .. 'maximized_normal_active_hover.svg'
	beautiful.titlebar_maximized_button_focus_active_hover  = tip .. beautiful.titlebar_theme .. '/' .. 'maximized_focus_active_hover.svg'
end)

return {
	theme = theme,
	awesome_overrides = awesome_overrides
}
