local awful = require('awful')
local beautiful = require('beautiful')
local icon_helper = require("library.bling_icon_theme")(beautiful.icon_theme)
local dpi = beautiful.xresources.apply_dpi

return {
	{
		type = [[Internet Tagged Desktop]],
		icon = icon_helper:get_icon_path("web-browser"),
		default_app = "xdg-open http://",
		gap = beautiful.useless_gap,
		layout = awful.layout.suit.floating
	},
	{
		type = [[Coding Tagged Desktop]],
		icon = icon_helper:get_icon_path("gnome-builder"),
		default_app = "gedit",
		gap = beautiful.useless_gap,
		layout = awful.layout.suit.floating
	},
	{
		type = [[Files Tagged Desktop]],
		icon = icon_helper:get_icon_path("nautilus"),
		default_app = "nautilus",
		gap = beautiful.useless_gap,
		layout = awful.layout.suit.tile
	},
	{
		type = [[Multimedia Tagged Desktop]],
		icon = icon_helper:get_icon_path("totem"),
		default_app = "totem",
		gap = beautiful.useless_gap,
		layout = awful.layout.suit.floating
	},
	{
		type = [[Gaming Tagged Desktop]],
		icon = icon_helper:get_icon_path("lutris"),
		default_app = "",
		gap = beautiful.useless_gap,
		layout = awful.layout.suit.max
	},
	{
		type = [[Graphics Tagged Desktop]],
		icon = icon_helper:get_icon_path("krita"),
		default_app = "gimp",
		gap = beautiful.useless_gap,
		layout = awful.layout.suit.floating
	}
}
