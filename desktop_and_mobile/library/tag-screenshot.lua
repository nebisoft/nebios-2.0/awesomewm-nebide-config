local gears = require("gears")
local cairo = require("lgi").cairo
local awful = require("awful")

-- Cache for storing screenshots of each tag
local tag_screenshot_cache = {}
local image
local last_image_path

function TagScreenshot(ctag)
    local tag = ctag
    
    local home_directory = os.getenv("HOME")
    local file_path = home_directory .. "/.current_wallpaper"
    local file = io.open(file_path, "r")
    local image_path = "/System/Wallpapers/Image/Galata (Day) by Ahmet Polat.jpg"
    if file then
	    local content = file:read("*all")
	    file:close()
	    image_path = content
    end
    
    local skip_cache = false
    if last_image_path == image_path then else
        last_image_path = image_path
        skip_cache = true
        tag_screenshot_cache = {}
    end

    -- Check if the screenshot is already cached for this tag
    if tag_screenshot_cache[tag] and skip_cache == false then
        -- Check if the client count has changed
        if #tag:clients() == tag_screenshot_cache[tag].client_count then
            return tag_screenshot_cache[tag].screenshot
        end
    end

    local screen = tag.screen

    -- Create a Cairo surface with scaled dimensions
    local scale_factor = 0.1 -- You can adjust this factor
    local scaled_width = math.floor(screen.geometry.width * scale_factor)
    local scaled_height = math.floor(screen.geometry.height * scale_factor)
    local surface = cairo.ImageSurface.create(cairo.Format.ARGB32, scaled_width, scaled_height)
    local cr = cairo.Context.create(surface)

    -- Load the specified image and scale it to fit the screen
    if skip_cache == true then
    	image = gears.surface.load(last_image_path)
    end
    
    local scale_x = scaled_width / image:get_width()
    local scale_y = scaled_height / image:get_height()

    -- Draw the scaled image onto the main surface
    cr:save()
    cr:scale(scale_x, scale_y)
    cr:set_source_surface(image, 0, 0)
    cr:paint()
    cr:restore()

    -- Iterate through clients in the specified tag
    for _, c in ipairs(tag:clients()) do
        -- Check if the client has a content surface and exclude specific class
        if c.content and c.class ~= "NebiDE_WE" then
            local client_surface = gears.surface(c.content)
            
            -- Calculate the scaled size of the client content
            local client_scaled_width = math.floor(c:geometry().width * scale_factor)
            local client_scaled_height = math.floor(c:geometry().height * scale_factor)

            -- Get the scaled client's position on the screen
            local x = math.floor((c:geometry().x - screen.geometry.x) * scale_factor)
            local y = math.floor((c:geometry().y - screen.geometry.y) * scale_factor)

            -- Draw the scaled client's content onto the main surface
            cr:save()
            cr:translate(x, y)
            cr:scale(scale_factor, scale_factor) -- Scale the context for the client
            cr:set_source_surface(client_surface, 0, 0)
            cr:paint()
            cr:restore()
        end
    end

    -- Save the composite surface to a file (you can customize the path and format)
    local screenshot_surface = gears.surface(surface)

    -- Update the cache with the new screenshot and client count
    tag_screenshot_cache[tag] = {
        screenshot = screenshot_surface,
        client_count = #tag:clients()
    }

    return screenshot_surface
end

return TagScreenshot

