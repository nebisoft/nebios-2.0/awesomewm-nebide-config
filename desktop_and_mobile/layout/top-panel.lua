local awful = require('awful')
local beautiful = require('beautiful')
local wibox = require('wibox')
local gears = require('gears')
local icons = require('theme.icons')
local dpi = beautiful.xresources.apply_dpi
local tag_list = require('widget.tag-list')
local clickable_container = require('widget.clickable-container')
awesome.register_xproperty("WM_NAME", "string")

separator =  wibox.widget {
		orientation = 'vertical',
		forced_height = dpi(1),
		forced_width = dpi(1),
		span_ratio = 0.5,
		widget = wibox.widget.separator
	}

local appname_gen = function(text)
	ib = wibox.widget.imagebox()
			tb = wibox.widget.textbox()
			bgb = wibox.container.background()
			tbm = wibox.widget {
				tb,
				left = dpi(4),
				right = dpi(8),
				widget = wibox.container.margin
			}
			ibm = wibox.widget {
				ib,
				margins = dpi(9),
				widget = wibox.container.margin
			}
			l = wibox.layout.fixed.horizontal()
			
			l:fill_space(true)
			l:add(ibm)
			l:add(tbm)
			
			tb:set_markup(text)
			
			bgb:set_widget(l)
			
			return bgb
		end
		
local appname = appname_gen("-")

client.connect_signal("focus", function(c)
	client.emit_signal("property::name", c)
end
)
client.connect_signal("property::name", function(c)
	ib.image = c.icon
	tb:set_markup(c.name)
end
)
client.connect_signal("unfocus", function(c)
	ib.image = nil
	tb:set_markup("·")
end
)

local top_panel = function(s)
	local panel = awful.wibar
	{
		ontop = true,
		screen = s,
		type = 'dock',
		height = dpi(36),
		stretch = true,
		bg = "#00000000",
		widget = {}
	}

	panel:connect_signal(
		'mouse::enter',
		function() 
			local w = mouse.current_wibox
			if w then
				w.cursor = 'left_ptr'
			end
		end
	)

	s.systray = wibox.widget {
		visible = true,
		base_size = dpi(24),
		margins = { left = dpi(24) },
		horizontal = true,
		screen = 'primary',
		widget = wibox.widget.systray
	}
	
	local clock 			= require('widget.clock')(s)
	local layout_box 		= require('widget.layoutbox')(s)
	s.tray_toggler  		= require('widget.tray-toggle')
	-- s.updater 				= require('widget.nebios-updater')()
	s.battery     			= require('widget.battery')()
	s.network       		= require('widget.network')()

	panel : setup {
		layout = wibox.layout.align.horizontal,
		expand = 'none', 
		 { -- Left widgets
            layout = wibox.layout.fixed.horizontal,
	    
			tag_list(s),
			separator,
			appname
        },
		{ --Middle widgets
			layout = wibox.layout.fixed.horizontal
		},
		{
			widget = wibox.container.background,
			bg = "#000000",
			shape = function(cr, width, height)
				gears.shape.partially_rounded_rect(
					cr, width, height, false, false, false, true, dpi(14)
				) 
			end,
			{
				layout = wibox.layout.fixed.horizontal,
				spacing = 0,
				{
					s.systray,
					margins = {
						top = dpi(8),
						bottom = dpi(8),
						left = dpi(8),
						right = 0
					},
					widget = wibox.container.margin
				},
				layout_box,
				s.battery,
				clock
			}
		}
	}

	panel:set_xproperty("WM_NAME", "NebiDE_top")
	
	screen.connect_signal("removed", function(s)
	    if s == panel.screen then
		    panel.visible = false
		    panel = nil
	    end
	end)

	return panel
end


return top_panel
