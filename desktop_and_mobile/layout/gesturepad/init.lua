local awful = require("awful")
local wibox = require("wibox")
local gears = require("gears")
local beautiful = require('beautiful')
local dpi = beautiful.xresources.apply_dpi
awesome.register_xproperty("WM_NAME", "string")

local gp_init = function(s)
	-- Get the screen dimensions
	local pad_width = s.geometry.width / 5
	local pad_height = dpi(12)

	-- Create the gesture bar
	local gesturepad = wibox({
	    height = pad_height,
	    width = dpi(pad_width),
	    screen = s,
	    x = s.geometry.width / 2 - (pad_width / 2),
	    y = s.geometry.height - dpi(12),
	    bg = "#00000000",
	    visible = true,
	    ontop = true,
	    type = "dock",
	    stretch = true,
	})

	-- Add the text to the gesture bar
	gesturepad:setup {
	    wibox.container.margin (
			wibox.container.background (
				wibox.container.margin (
					wibox.container.background (
						nil,
						"#535353",
						function(cr, width, height)
							gears.shape.partially_rounded_rect(
								cr, width, height, true, true, true, true, dpi(3)
							) 
						end
					),
					dpi(1),
					dpi(1),
					dpi(1),
					dpi(1)
				),
				"#00000099",
				function(cr, width, height)
					gears.shape.partially_rounded_rect(
						cr, width, height, true, true, true, true, dpi(3)
					) 
				end
			),
			dpi(3),
			dpi(3),
			dpi(3),
			dpi(3)
		),
	    layout = wibox.layout.flex.horizontal,
	}
	
	gesturepad:set_xproperty("WM_NAME", "NebiDE_gesturepad")
	
	gesturepad:connect_signal("mousegrabber::stop", function() gesturepad.y = s.geometry.height - gesturepad.height end)

	-- Attach the mouse button signals to the gesture bar
	gesturepad:connect_signal("button::press", function(_, _, _, button)
	    if button == 1 then
	    	os.execute("picom-compositor &")
	    	client_focus_was = client.focus
	    	client.focus = nil
	    	c = client_focus_was
		-- Begin mouse grabber
		local grabber = function(m)
		    local centerX = s.geometry.width / 2
		    local mouseDistance = m.x - (centerX / 2) - gesturepad.width - (gesturepad.width / (2 * 2))
		    local maxDistance = centerX - gesturepad.width / 2
		    local xPercentage = mouseDistance / maxDistance
		    
		    if m.buttons[1] then
			local leftStrut = math.max((s.geometry.height - gesturepad.y) / 3 * 4 + (mouseDistance / 4), 0)
			local rightStrut = math.max((s.geometry.height - gesturepad.y) / 3 * 4 - (mouseDistance / 4), 0)
			gesturepad:struts {
			    top = s.geometry.height - gesturepad.y + dpi(36),
			    bottom = s.geometry.height - gesturepad.y,
			    left = leftStrut,
			    right = rightStrut
			}
		    else
		    	    gesturepad:struts {
			    	top = 0,
			    	bottom = 0,
			    	left = 0,
			    	right = 0
			    }
		    end
		    
		    if m.y <= s.geometry.height - gesturepad.height and m.y >= (s.geometry.height / 1.35) and m.x >= (s.geometry.width / 4) and m.x <= (s.geometry.width / 4 * 3)then
			    gesturepad.y = m.y - (gesturepad.height / 2) + (s.geometry.height % m.y / 4)
			    gesturepad.x = centerX - (gesturepad.width / 2) + (maxDistance * xPercentage / 2)
		    else
		    	    gesturepad.y = s.geometry.height - gesturepad.height
		    	    gesturepad.x = (s.geometry.width / 2) - (gesturepad.width / 2)
			    if m.x <= (s.geometry.width / 4) then
			    	    mousegrabber.stop()
			    	    awful.client.focus.byidx(1)
			    end
			    if m.x <= (s.geometry.width / 4) then
			    	    mousegrabber.stop()
			    	    awful.client.focus.byidx(-1)
			    end
		    	    if m.y <= (s.geometry.height / 1.35) then
			    	    mousegrabber.stop()
			    	    for _, c in ipairs(client.get()) do
					if not c.skip_taskbar and c.type ~= "desktop" then
					    c.minimized = true
					end
				    end
			    end
		    end
		    
		    if gesturepad.y >= s.geometry.height - dpi(10 + beautiful.action_bar_icon_size) then
		    	awesome.emit_signal("bottom_panel::is_shown", false)
		    else
		    	awesome.emit_signal("bottom_panel::is_shown", true)
		    	focus_to_client_at_stop = true
		    end
		    
		    if m.buttons[1] then
		    	return true
		    else
		    	gesturepad:struts {
			    	top = 0,
			    	bottom = 0,
			    	left = 0,
			    	right = 0
			}
		    	gesturepad.y = s.geometry.height - gesturepad.height
		    	gesturepad.x = (s.geometry.width / 2) - (gesturepad.width / 2)
		    	awesome.emit_signal("bottom_panel::is_shown", true)
			if focus_to_client_at_stop == true then
				client.focus = client_focus_was
			end
		    	return false
		    end
		end
		mousegrabber.run(grabber, "fleur")
	    end
	end)
end

return gp_init
