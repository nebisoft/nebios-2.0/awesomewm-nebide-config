local wibox = require('wibox')
local awful = require('awful')
local gears = require('gears')
local beautiful = require('beautiful')
local dpi = beautiful.xresources.apply_dpi
local apps = require('configuration.apps')
local action_bar = require('layout.left-panel.action-bar')
local action_bar_icon_size = beautiful.action_bar_icon_size
awesome.register_xproperty("WM_NAME", "string")

local left_panel = function(screen)
	
	local action_bar_width = dpi(10 + action_bar_icon_size)
	local panel_content_width = dpi(350)

	local panel = wibox {
		ontop = true,
		screen = screen,
		type = 'dock',
		margins = {
			left = dpi(10),
			right = dpi(10),
			top = dpi(10),
			bottom = dpi(10)
		},
		width = action_bar_width,
		bg = beautiful.background,
		fg = beautiful.fg_normal,
		shape           = function(cr, width, height)
				gears.shape.partially_rounded_rect(
					cr, width, height, true, true, true, true, dpi(12)
				)
			end
	}
	
	panel.width = action_bar_width
	panel.y = screen.geometry.y + dpi(10)
	panel.x = screen.geometry.x + dpi(10)
	panel.height = screen.geometry.height - dpi(20)

	panel.opened = false

	panel:struts {
		left = dpi(45 + 20)
	}

	local backdrop = wibox {
		ontop = true,
		screen = screen,
		bg = beautiful.transparent,
		type = 'utility',
		x = screen.geometry.x + dpi(10) + panel_content_width,
		y = screen.geometry.y,
		width = screen.geometry.width - panel_content_width,
		height = screen.geometry.height
	}

	function panel:run_rofi()
		awesome.spawn(
			apps.default.rofi_global,
			false,
			false,
			false,
			false,
			function()
				panel:toggle()
			end
		)
		
		-- Hide panel content if rofi global search is opened
		panel:get_children_by_id('panel_content')[1].visible = false
	end

	-- "Punch a hole" on backdrop to show the left dashboard
	local update_backdrop = function(wibox_backdrop, wibox_panel)
		local cairo = require('lgi').cairo
		local geo = wibox_panel.screen.geometry

		wibox_backdrop.x = geo.x
		wibox_backdrop.y = geo.y
		wibox_backdrop.width = geo.width
		wibox_backdrop.height = geo.height

		-- Create an image surface that is as large as the wibox_panel screen
		local shape = cairo.ImageSurface.create(cairo.Format.A1, geo.width, geo.height)
		local cr = cairo.Context(shape)

		-- Fill with "completely opaque"
		cr.operator = 'SOURCE'
		cr:set_source_rgba(1, 1, 1, 1)
		cr:paint()

		-- Remove the shape of the client
		local c_geo = wibox_panel:geometry()
		local c_shape = gears.surface(wibox_panel.shape_bounding)
		cr:set_source_rgba(0, 0, 0, 0)
		cr:mask_surface(c_shape, c_geo.x + wibox_panel.border_width - geo.x, c_geo.y + wibox_panel.border_width - geo.y)
		c_shape:finish()

		wibox_backdrop.shape_bounding = shape._native
		shape:finish()
		wibox_backdrop:draw()
	end

	local open_panel = function(should_run_rofi)
		panel.width = action_bar_width + panel_content_width
		update_backdrop(backdrop, panel)
		panel:get_children_by_id('panel_content')[1].visible = true
		backdrop.x = screen.geometry.x + dpi(10) + panel.width
		backdrop.width = screen.geometry.width - panel_content_width
		backdrop.visible = true
		if should_run_rofi then
			panel:run_rofi()
		end
		panel:emit_signal('opened')
	end

	local close_panel = function()
		panel.width = action_bar_width
		panel:get_children_by_id('panel_content')[1].visible = false
		backdrop.visible = false
		update_backdrop(backdrop, panel)
		panel:emit_signal('closed')
	end

	-- Hide this panel when app dashboard is called.
	function panel:hide_dashboard()
		close_panel()
	end

	backdrop:buttons(
		awful.util.table.join(
			awful.button(
				{},
				1,
				function()
					panel:toggle()
				end
			)
		)
	)

	panel:setup {
		layout = wibox.layout.fixed.horizontal,
		action_bar(screen, panel, action_bar_width)
	}
	
	panel:set_xproperty("WM_NAME", "NebiDE_left")
	return panel
end

return left_panel
