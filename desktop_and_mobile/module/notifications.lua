local gears = require('gears')
local wibox = require('wibox')
local awful = require('awful')
local ruled = require('ruled')
local naughty = require('naughty')
local menubar = require('menubar')
local beautiful = require('beautiful')
local dpi = beautiful.xresources.apply_dpi
local clickable_container = require('widget.clickable-container')

-- Defaults
naughty.config.defaults.ontop = true
naughty.config.icon_size = dpi(48)
naughty.config.defaults.timeout = 5
naughty.config.defaults.title = 'System Notification'
naughty.config.defaults.margin = 0
naughty.config.defaults.border_width = 0
naughty.config.defaults.position = 'bottom_right'
naughty.config.defaults.shape = function(cr, w, h)
	gears.shape.rounded_rect(cr, w, h, dpi(4))
end

-- Apply theme variables
naughty.config.padding = dpi(0)
naughty.config.spacing = dpi(0)
naughty.config.icon_dirs = {
	'/usr/share/icons/Tela',
	'/usr/share/icons/Tela-blue-dark',
	'/usr/share/icons/Papirus/',
	'/usr/share/icons/la-capitaine-icon-theme/',
	'/usr/share/icons/gnome/',
	'/usr/share/icons/hicolor/',
	'/usr/share/pixmaps/'
}
naughty.config.icon_formats = { 'svg', 'png', 'jpg', 'gif' }


-- Presets / rules

ruled.notification.connect_signal(
	'request::rules',
	function()

		-- Critical notifs
		ruled.notification.append_rule {
			rule       = { urgency = 'critical' },
			properties = { 
				font        		= beautiful.font,
				bg 					= '#ff5f5f', 
				fg 					= '#ffffff',
				margin 				= dpi(16),
				position 			= 'bottom_right',
				implicit_timeout	= 0
			}
		}

		-- Normal notifs
		ruled.notification.append_rule {
			rule       = { urgency = 'normal' },
			properties = {
				font        		= beautiful.font,
				bg      			= beautiful.transparent, 
				fg 					= beautiful.fg_normal,
				margin 				= dpi(16),
				position 			= 'bottom_right',
				implicit_timeout 	= 5
			}
		}

		-- Low notifs
		ruled.notification.append_rule {
			rule       = { urgency = 'low' },
			properties = { 
				font        		= beautiful.font,
				bg     				= beautiful.transparent,
				fg 					= beautiful.fg_normal,
				margin 				= dpi(16),
				position 			= 'bottom_right',
				implicit_timeout	= 5
			}
		}
	end
	)

-- Locales

local lang = os.getenv("LANG")

local foldername = "Feedback Data"
local ErrorHappened =  'An Error Occurred!'
local LookAtLogs =  'Please create a feedback to NebiSoft with including the latest created error log file in "User Home/Feedback Data".'

if lang == "tr_TR.UTF-8" then
	foldername = "Geri Bildirim Verisi"
	ErrorHappened =  'Bir Hata Oluştu!'
	LookAtLogs =  'Lütfen NebiSoft\'a "Kullanıcı Evi/Geri Bildirim Verisi" klasöründeki en son oluşturulmuş hata günlüğü dosyasını içeren bir geri bildirim oluşturun.'
end

-- Error handling
naughty.connect_signal(
	'request::display_error',
	function(message, startup)
		naughty.notification ({
            app_name = 'System notification',
            title = ErrorHappened,
            message = LookAtLogs,
            urgency = 'critical'
        })
        awful.spawn("play /System/Sounds/Dunk.mp3 &")
		awful.spawn.with_shell('mkdir "$HOME/' .. foldername .. '"; FILE="$HOME/' .. foldername .. '/$(date +NebiDE_Error___%Y-%m-%d___%H:%M:%S:%N.log)"; NEBIDE_VER=$(apt show nebide | grep Version | sed "s+.* ++g"); echo "# " > $FILE; echo "# NebiDE $NEBIDE_VER (c) 2020-$(date +%Y) NebiSoft. Some Rights Reserved." >> $FILE;  echo "# " >> $FILE; echo >> $FILE; echo "... # Probably works as excepted." >> $FILE; echo >> $FILE; echo "' .. message .. '" >> $FILE; echo "EOF" >> $FILE;')
	end
)

-- XDG icon lookup
naughty.connect_signal(
	'request::icon',
	function(n, context, hints)
		if context ~= 'app_icon' then return end

		local path = menubar.utils.lookup_icon(hints.app_icon) or
		menubar.utils.lookup_icon(hints.app_icon:lower())

		if path then
			n.icon = path
		end
	end
)

-- Connect to naughty on display signal
naughty.connect_signal(
	'request::display',
	function(n)
	n.title = "<b>" .. n.title .. "</b>"
		-- Actions Blueprint
		local actions_template = wibox.widget {
			notification = n,
			base_layout = wibox.widget {
				spacing        = dpi(0),
				layout         = wibox.layout.flex.horizontal
			},
			widget_template = {
				{
					{
						{
							{
								id     = 'text_role',
								font   = beautiful.font,
								widget = wibox.widget.textbox
							},
							widget = wibox.container.place
						},
						widget = clickable_container
					},
					bg                 = beautiful.groups_bg,
					shape              = function(cr, w, h)
	gears.shape.rounded_rect(cr, w, h, dpi(6))
end,
					forced_height      = dpi(30),
					widget             = wibox.container.background
				},
				margins = dpi(4),
				widget  = wibox.container.margin
			},
			style = { underline_normal = false, underline_selected = true },
			widget = naughty.list.actions
		}

		-- Notifbox Blueprint
		naughty.layout.box {
			notification = n,
			type = 'notification',
			screen = awful.screen.preferred(),
			shape = gears.shape.rectangle,
			widget_template = {
				{
					{
						{
							{
								{
									{
										{
											{
												{
													{
														resize_strategy = 'center',
														widget = naughty.widget.icon {shape = function(cr, w, h)
	gears.shape.rounded_rect(cr, w, h, dpi(4))
end},
														
													},
													bottom = 0,
													right = dpi(10),
													widget  = wibox.container.margin,
												},
												{
													{
														layout = wibox.layout.align.vertical,
														expand = 'none',
														nil,
														{
															{
																align = 'left',
																font   = beautiful.font,
																widget = naughty.widget.title {font   = beautiful.font}
															},
															{
																align = 'left',
																widget = naughty.widget.message,
															},
															layout = wibox.layout.fixed.vertical
														},
														nil
													},
													widget  = wibox.container.margin,
												},
												layout = wibox.layout.fixed.horizontal,
											},
											fill_space = false,
											spacing = beautiful.notification_margin,
											layout  = wibox.layout.fixed.vertical,
										},
										-- Margin between the fake background
										-- Set to 0 to preserve the 'titlebar' effect
										margins = dpi(10),
										widget  = wibox.container.margin,
									},
									bg = beautiful.transparent,
									widget  = wibox.container.background,
								},
								-- Actions
								actions_template,
								spacing = dpi(0),
								layout  = wibox.layout.fixed.vertical,
							},
							bg     = beautiful.transparent,
							id     = 'background_role',
							widget = naughty.container.background,
						},
						strategy = 'min',
						width    = dpi(300),
						widget   = wibox.container.constraint,
					},
					strategy = 'max',
					width    = dpi(300),
					widget   = wibox.container.constraint
				},
				bg = beautiful.background,
				shape = gears.shape.rounded_rect,
				widget = wibox.container.background
			}
		}
	
		-- Destroy popups if dont_disturb mode is on
		-- Or if the right_panel is visible
		local focused = awful.screen.focused()
		if _G.dont_disturb or
			(focused.right_panel and focused.right_panel.visible) then
			naughty.destroy_all_notifications(nil, 1)
		end

	end
)
