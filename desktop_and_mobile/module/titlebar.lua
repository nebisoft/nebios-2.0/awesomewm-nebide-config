local awful = require('awful')
local gears = require('gears')
local beautiful = require('beautiful')
local wibox = require('wibox')
local icons = require('theme.icons')
local dpi = beautiful.xresources.apply_dpi
local ems = require('module.ems')
local lgi = require("lgi")
local cairo = lgi.cairo
local gdk = lgi.Gdk
local get_default_root_window = gdk.get_default_root_window
local pixbuf_get_from_surface = gdk.pixbuf_get_from_surface
local pixbuf_get_from_window = gdk.pixbuf_get_from_window
awful.titlebar.enable_tooltip = true
awful.titlebar.fallback_name  = 'Client'
awful.client.property.disable_focus_transient_for = true

local double_click_event_handler = function(double_click_event)
	if double_click_timer then
		double_click_timer:stop()
		double_click_timer = nil
		double_click_event()
		return
	end
	double_click_timer = gears.timer.start_new(
		0.20,
		function()
			double_click_timer = nil
			return false
		end
	)
end

local create_click_events = function(c)
	-- Titlebar button/click events
	local my_c = c
	local buttons = gears.table.join(
		awful.button(
			{},
			1,
			function()
				double_click_event_handler(function()
					if c.floating then
						c.floating = false
						return
					end
					c.maximized = not c.maximized
					c:raise()
					return
				end)
				c:activate {context = 'titlebar', action = 'mouse_move'}
			end
		),
		awful.button(
			{},
			3,
			function()
				local ontop_icon = icons.toggled_off
				if c.ontop == true then
			    		ontop_icon = icons.toggled_on
			    	end
			    	
			    	my_c:raise()
			    	
				local floating_icon = icons.toggled_off
				if c.floating == true then
			    		floating_icon = icons.toggled_on
			    	end
			    	
				local menu = awful.menu({
				
				theme = {
					height = dpi(30)
				},
				
				items = {
				    { "  Close", function() my_c:kill() end, beautiful.titlebar_close_button_focus },
				    { "  Maximize", function() my_c.maximized = not my_c.maximized end, beautiful.titlebar_maximized_button_focus_active },
				    { "  Minimize", function() my_c.minimized = true end, beautiful.titlebar_minimize_button_focus },
				    { "  Forced floating", function() my_c.floating = not my_c.floating end, floating_icon },
				    { "  Always in top", function() my_c.ontop = not my_c.ontop end, ontop_icon }
				}
				
				})
				
				ems(menu)
			end
		)
	)
	return buttons
end

local create_titlebar = function(c, pos, tsize)
	local all_pos = {
		"left", "right", "top", "bottom"
	}
	
	for i, item in ipairs(all_pos) do
		if all_pos[i] == pos then
			table.remove(all_pos, i)
		end
	end
	
	local orientation = wibox.layout.fixed.vertical
	local align = wibox.layout.align.vertical
	local flex = wibox.layout.flex.vertical
	
	if pos == "left" or pos == "right" then
		orientation = wibox.layout.fixed.vertical
		align = wibox.layout.align.vertical
		flex = wibox.layout.flex.vertical
	else
		orientation = wibox.layout.fixed.horizontal
		align = wibox.layout.align.horizontal
		flex = wibox.layout.flex.horizontal
	end
	
	local lay_widname = {}
	
	local titlebar_layout = beautiful.titlebar_layout
	
	local index = 1
	for sub_lay in titlebar_layout:gmatch("([^:]*)") do
	  	lay_widname[index] = sub_lay
	  	index = index + 1
	end
	
	local lay_widarr = {}
	
	local widget_map = {}
	widget_map["i"] = awful.titlebar.widget.iconwidget(c)
	widget_map["c"] = awful.titlebar.widget.closebutton(c)
	widget_map["x"] = awful.titlebar.widget.maximizedbutton(c)
	widget_map["m"] = awful.titlebar.widget.minimizebutton(c)
	widget_map["t"] = awful.titlebar.widget.titlewidget(c)
	
	for i in pairs(lay_widname) do
		local item = lay_widname[i]
		local wid_array = { spacing = dpi(7), layout = orientation, wibox.widget.base.empty_widget() }
		for i = 1, #item do
			local char = string.sub(item, i, i)
			local widget = widget_map[char]
			if widget == nil then else
				table.insert(wid_array, widget)
			end
		end
		table.insert(lay_widarr, wid_array)
	end
	
	local click_events = create_click_events(c)
	
	local final_wid = {orientation(),orientation(),orientation()}
	
	final_wid[1].spacing = dpi(7)
	final_wid[2].spacing = dpi(7)
	final_wid[3].spacing = dpi(7)
	
	for i in pairs(lay_widarr) do
		final_wid[i]:add(table.unpack(lay_widarr[i]))
	end
	
	end_margin = {
		top = dpi(10),
		left = dpi(10),
		right = dpi(10),
		bottom = dpi(10)
	}
	if pos == "left" or pos == "right" then
	 	end_margin.bottom = dpi(2)
	else
	 	end_margin.right = dpi(4)
	end

	awful.titlebar(c, {position = pos,  size = dpi(tsize)}) : setup {
		{
			final_wid[1],
			margins = dpi(10),
			widget = wibox.container.margin
		},
		{
			{
				{
					final_wid[2],
					expand = "inside",
					layout = orientation
				},
				halign = "center",
				valign = "center",
				widget = wibox.container.place
			},
			margins = dpi(10),
			buttons = click_events,
			widget = wibox.container.margin
		},
		{
			final_wid[3],
			margins = end_margin,
			widget = wibox.container.margin
		},
		layout = align
	}
	
	widget_map["t"].align = "center"
	widget_map["t"].font = beautiful.font
end

local update_panel_visiblity = function(cl)
	if cl.screen == screen.primary then
		if beautiful.bottom_panel_hide_policy == "smart" then
			local statement = true
			local scr = cl.screen

			if screen.primary == scr then
			    local height_of_bars = dpi(20 + beautiful.action_bar_icon_size)
			    local scrend_x = scr.geometry.x + scr.geometry.width
			    local scrend = scr.geometry.y + scr.geometry.height

			    local placement = string.sub(beautiful.bottom_panel_placement or "btm", 1, 3)
			    if placement == "lft" then
				statement = cl.x < scr.geometry.x + dpi(10) + height_of_bars
			    elseif placement == "btm" then
				statement = cl.y + cl.height > scrend - dpi(10) - height_of_bars
			    elseif placement == "rig" then
				statement = cl.x + cl.width > scrend_x - dpi(10) - height_of_bars
			    end
			end
			
			awesome.emit_signal("bottom_panel::is_shown", not statement)
		end
	else
		gears.timer.start_new(1, function()
		        awesome.emit_signal("bottom_panel::is_shown", true)
		    end)
	end
end

client.connect_signal('property::position', function(c)	
	if c.type == "desktop" then else
		print("update_panel_visiblity run")
		gears.timer.start_new(1, function()
  			update_panel_visiblity(c)
  		end)
		collectgarbage()
	end
end)

client.connect_signal('property::size', function(c)	
	if c.type == "desktop" then else
		print("update_panel_visiblity run")
		gears.timer.start_new(1, function()
  			update_panel_visiblity(c)
  		end)
		collectgarbage()
	end
end)

client.connect_signal('unfocus', function(c)	
	local scr = c.screen
	if beautiful.bottom_panel_hide_policy == "smart" then
		awesome.emit_signal("bottom_panel::is_shown", true)
	end
	os.execute("nebide-optimizer-exit &")
	collectgarbage()
end)

client.connect_signal(
	'focus',
	function(c)
		if c.type == "desktop" then else
			print("update_panel_visiblity run")
		  	update_panel_visiblity(c)
			
			local diff_width = c.width - c.screen.geometry.width
			local diff_height = c.height - c.screen.geometry.height
			local diff = diff_width + diff_height
			if diff == 23 or diff == 56 or diff == 36 then
				diff = 0
			end
			-- os.execute("echo " .. c.pid .. " " .. diff .. " >> ~/lastwin")
			local diff_if = c.screen.geometry.x == 0 and c.screen.geometry.y == 0 and diff == 0
			if c.fullscreen or diff_if then
				c.fullscreen = true
			end
			os.execute("bash -c 'sleep 1; nebide-optimizer " .. tostring(c.pid) .. " " .. tostring(c.fullscreen) .. "' &")
		end
			
		collectgarbage()
	end
)

awesome.connect_signal("titlebar::update_conf", function(pos, lay, icon_theme, size)	
	beautiful.titlebar_position = pos
	beautiful.titlebar_layout = lay
	beautiful.titlebar_theme = icon_theme
	if size == nil then else
		beautiful.titlebar_size = size
	end
	awesome.emit_signal('titlebar::update_icon_theme_vars')
	collectgarbage()
end)

client.connect_signal(
	'request::titlebars',
	function(c, context)
--		awful.spawn.easy_async_with_shell([[
--		
--			if [ -z "$(xprop -id ]] .. c.window .. [[ | grep _GTK_APPLICATION_ID)" ]; then
--				echo false;
--			else
--				echo true;
--			fi
--			
--		]], function(out)
--			if (out:match("true") and c.requests_no_titlebar == true) or c.requests_no_titlebar == false then
			if c.requests_no_titlebar == false or c.force_titlebars_enabled == true then
				-- Customize here
				if c.type == 'normal' then

					create_titlebar(c, beautiful.titlebar_position, beautiful.titlebar_size)
					c.border_width = dpi(1)

				elseif c.type == 'dialog' then
					create_titlebar(c, beautiful.titlebar_position, beautiful.titlebar_size)
					c.border_width = 0

				elseif c.type == 'modal' then
					create_titlebar(c, beautiful.titlebar_position, beautiful.titlebar_size)
					c.border_width = dpi(1)

				else
					create_titlebar(c, beautiful.titlebar_position, beautiful.titlebar_size)
					c.border_width = dpi(1)
				end
			else
				if c.force_titlebars_enabled == true then
					c.border_width = dpi(1)
				else
					c.border_width = 0
				end
			end
--		end)
	end
)
