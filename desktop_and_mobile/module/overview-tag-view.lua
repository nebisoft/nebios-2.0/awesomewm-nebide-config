local awful = require("awful")
local gears = require("gears")
local beautiful = require("beautiful")
local tag_preview = require("library.tag-screenshot")
local dpi = beautiful.xresources.apply_dpi
local clickable_container = require('widget.clickable-container')
local wibox = require("wibox")

local function TagPopup(ourscreen)
    local tag_popup = wibox {
        visible = false,
        ontop = true,
        bg = '#00000066',
        border_color = '#00000000',
        border_width = dpi(1),
        screen = ourscreen,
    }
    
	tag_popup.width = dpi(128)
        tag_popup.height = dpi(36)
        
    local textbox = wibox.widget {
                widget = wibox.container.margin,
		    margins = dpi(8),
		    {
		        widget = wibox.widget.textbox,
		        text = "Show tags",
		        visible = true,
		        align = "center"
		    }
            }
            
    local tag_buttons = wibox.widget{
            visible = false,
            layout = wibox.layout.flex.horizontal
        }
        
    local function onTagButtonClick(tag_num)
    	    mousegrabber.stop()
	    awesome.emit_signal("expose::exit")
	    awful.tag.viewonly(tag_num)
	end
        
    local function createTagButton(tag_num)
    	    local t_image = wibox.widget {
		                widget = wibox.widget.imagebox,
		                -- Use a blank image as a placeholder for now
		                maximum_height = dpi(96),
		                resize = true
		            }
	    local button = wibox.widget {
	        widget = wibox.container.margin,
				margins = dpi(1),
	        {
		    	widget = clickable_container,
			border_color = '#fffff66',
			border_width = dpi(2),
			shape = function(cr, width, height) gears.shape.partially_rounded_rect(cr, width, height, true, true, true, true, dpi(12)) end,
			{
				widget = wibox.container.margin,
				margins = dpi(8),
				buttons = gears.table.join(
				    awful.button({}, 1, function() onTagButtonClick(tag_num) end)
				),
				{
				    widget = wibox.layout.fixed.vertical,
				    spacing = dpi(5),
				    {
					{widget = t_image},
				    	widget = wibox.container.background,
					shape = function(cr, width, height) gears.shape.partially_rounded_rect(cr, width, height, true, true, true, true, dpi(8)) end
				    },
				    {
					widget = wibox.widget.textbox,
					text = tag_num.name,
					align = "center"
				    },
				}
			}
		}
	    }

	    local function loadTagPreviewAsync()
		    t_image.image = tag_preview(tag_num)
		    collectgarbage("collect")
	    end

	    local co = coroutine.create(loadTagPreviewAsync)
	    local function resumeCoroutine()
		local status, error_message = coroutine.resume(co)
		if not status then
		    print("Coroutine error: " .. tostring(error_message))
		end
	    end

	    gears.timer.start_new(0, resumeCoroutine)

	    return button
	end
        
    tag_popup:setup {
        layout = wibox.layout.fixed.vertical,
        spacing = dpi(10),
        nil,
        {
            widget = textbox,
            visible = false
        },
        {
            widget = tag_buttons,
            visible = false
        }
    }

    -- Toggle visibility when mouse enters or leaves the popup
    tag_popup:connect_signal("mouse::enter", function()
        tag_popup.width = dpi(1000)
        tag_popup.height = dpi(128)
        awful.placement.top(tag_popup, { margins = dpi(10) })
        tag_popup:struts {
        	top = dpi(128 + 10 + 20),
		left = dpi(180 + beautiful.action_bar_icon_size),
		right = dpi(180 + beautiful.action_bar_icon_size)
        }
        textbox.visible = false
        tag_buttons.visible = true
        collectgarbage()
    end)

    tag_popup:connect_signal("mouse::leave", function()
        tag_popup.width = dpi(128)
        tag_popup.height = dpi(32)
        awful.placement.top(tag_popup, { margins = dpi(10) })
        tag_popup:struts {
        	top = 0,
		left = 0,
		right = 0
        }
        textbox.visible = true
        tag_buttons.visible = false
        if tag_popup.visible then
        	awesome.emit_signal("expose::run_mg")
        end
        collectgarbage()
    end)

    -- Update x when the popup becomes visible
    tag_popup:connect_signal("property::visible", function()
        if tag_popup.visible then
	    tag_popup.width = dpi(128)
            tag_popup.height = dpi(32)
            textbox.visible = true
            tag_buttons.visible = false
            awful.placement.top(tag_popup, { margins = dpi(10) })
	    tag_popup:struts {
		top = 0,
		left = 0,
		right = 0
            }
	    tag_buttons:reset()
	    for i, tag in ipairs(ourscreen.tags) do
		if not string.find(tag.name, "Window Overview") then
		    local tagButton = createTagButton(tag)
		    tag_buttons:add(tagButton)
		end
	    end
            collectgarbage()
        end
    end)

    return tag_popup
end

return TagPopup

