local wibox = require('wibox')
local awful = require('awful')
local naughty = require('naughty')
local gears = require('gears')

local clickable_container = require('widget.clickable-container')

local beautiful = require('beautiful')
local icon_helper = require("library.bling_icon_theme")(beautiful.icon_theme)
local dpi = beautiful.xresources.apply_dpi

local config_dir = gears.filesystem.get_configuration_dir()
local widget_icon_dir = config_dir .. 'widget/xdg-folders/icons/'

local lang = os.getenv("LANG")
local ems = require('module.ems')

local openTrash = 'Open trash'
local deleteForever = 'Delete forever'
local yes = 'Yes'
local no = 'No'
local tempty = 'Trash empty'
local tincl = 'Trash contains:'

if lang == "tr_TR.UTF-8" then
	openTrash = 'Çöpü aç'
	deleteForever = 'Sonsuza dek sil'
	yes = 'Evet'
	no = 'Hayır'
	tempty = 'Çöp boş'
	tincl = 'Çöp şunları içeriyor:'
end

local create_widget = function()
	local trash_widget = wibox.widget {
		{
			id = 'trash_icon',
			image = icon_helper:get_icon_path('user-trash-empty'),
			resize = true,
			widget = wibox.widget.imagebox
		},
		layout = wibox.layout.align.horizontal
	}

	local trash_menu = awful.menu({
		items = {
			{
				openTrash,
				function()
					awful.spawn.easy_async_with_shell(
						'gio open trash:///', 
						function(stdout) end,
						1
					) 
				end,
				icon_helper:get_icon_path('open-folder')
			},
			{
				deleteForever, 
				{
					{
						yes,
						function()
							os.execute('gio trash --empty & play /System/Sounds/Hypesweep.mp3 &')
						end,
						widget_icon_dir .. 'yes.svg'
					},
					{
						no,
						function()
							os.execute('echo')
						end,
						widget_icon_dir .. 'no.svg'
					}
				},
				icon_helper:get_icon_path('user-trash-empty')
			},
		}
	})

	local trash_button = wibox.widget {
		{
			{
				trash_widget,
				margins = dpi(4),
				widget = wibox.container.margin
			},
			shape = function(cr, width, height)
				gears.shape.rounded_rect(cr, width, height, dpi(12))
			end,
			widget = clickable_container
		},
		margins = dpi(4),
		widget = wibox.container.margin
	}

	-- Tooltip for trash_button
	trash_tooltip = awful.tooltip {
		objects = {trash_button},
		mode = "outside",
		margin_leftright = dpi(10),
		margin_topbottom = dpi(6),
		preferred_alignments = {'middle'},
		preferred_positions = {'top', 'right', 'left'},
		align = 'top',
		markup = 'Trash'
	}

	-- Mouse event for trash_button
	trash_button:buttons(
		gears.table.join(
			awful.button(
				{},
				1,
				nil,
				function()
					awful.spawn({'gio', 'open', 'trash:///'}, false)
				end
			),
			awful.button(
				{},
				3,
				nil,
				function()
					ems(trash_menu)
					trash_tooltip.visible = not trash_tooltip.visible
				end
			)
		)
	)

	-- Update icon on changes
	local check_trash_list = function()
		awful.spawn.easy_async_with_shell(
			'gio list trash:/// | wc -l',
			function(stdout) 
				if tonumber(stdout) > 0 then
					trash_widget.trash_icon:set_image(icon_helper:get_icon_path('user-trash-full'))

					awful.spawn.easy_async_with_shell(
						'gio list trash:///',
						function(stdout)
							trash_tooltip.markup = '<b>' .. tincl .. '</b>\n' .. stdout:gsub('\n$', '')
						end
					)
				else
					trash_widget.trash_icon:set_image(icon_helper:get_icon_path('user-trash'))
					trash_tooltip.markup = tempty
				end
			end
		)
	end

	-- Check trash on awesome (re)-start
	check_trash_list()

	-- Kill the old process of gio monitor trash:///
	awful.spawn.easy_async_with_shell(
		'ps x | grep \'gio monitor trash:///\' | grep -v grep | awk \'{print  $1}\' | xargs kill',
		function() 
			awful.spawn.with_line_callback(
				'gio monitor trash:///',
				{
					stdout = function(_)
						check_trash_list()
					end
				}
			)
		end
	)

	return trash_button
end

return create_widget
