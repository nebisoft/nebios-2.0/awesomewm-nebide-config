local wibox = require('wibox')
local awful = require('awful')
local gears = require('gears')
local beautiful = require('beautiful')
local dpi = beautiful.xresources.apply_dpi
local clickable_container = require('widget.clickable-container')
local icons = require('theme.icons')

local lang = os.getenv("LANG")

-- Locales
local launchErr =  'There is no application configured for execute.'

if lang == "tr_TR.UTF-8" then
	launchErr = "Hiçbir uygulama çalıştırılmak için ayarlanmamış."
end

local create_open_default_button = function(s) 
	s.add_button = wibox.widget {
		{
			{
				{
					{
						image = icons.plus,
						resize = true,
						widget = wibox.widget.imagebox
					},
					margins = dpi(10),
					widget = wibox.container.margin
				},
				widget = clickable_container
			},
			bg = beautiful.transparent,
			shape = gears.shape.circle,
			widget = wibox.container.background
		},
		margins = dpi(5),
		top = 0,
		widget = wibox.container.margin
	}

	s.add_button:buttons(
		gears.table.join(
			awful.button(
				{},
				1,
				nil,
				function()
					if awful.screen.focused().selected_tag.default_app == "" then
						os.execute("zenity --info --text=\"" .. launchErr .. "\" &")
					else
					awful.spawn(
						awful.screen.focused().selected_tag.default_app,
						{
							tag = mouse.screen.selected_tag
						}
					)
					end
				end
			)
		)
	)
	return s.add_button
end

return create_open_default_button
