local filesystem = require('gears.filesystem')
local beautiful = require('beautiful')
local theme_dir = filesystem.get_configuration_dir() .. '/theme'

local theme = {}

theme.icons = theme_dir .. '/icons/'
theme.font = beautiful.font
theme.font_bold = beautiful.font

-- Colorscheme
theme.system_black_dark = '#353535'
theme.system_black_light = '#535353'

theme.system_red_dark = '#ff5f5f'
theme.system_red_light = '#ff5f5f'

theme.system_green_dark = '#5CE0B6'
theme.system_green_light = '#5CE0B6'

theme.system_yellow_dark = '#F8C448'
theme.system_yellow_light = '#F8C448'

theme.system_blue_dark = '#6BCDCE'
theme.system_blue_light = '#6BCDCE'

theme.system_magenta_dark = '#9b59b6'
theme.system_magenta_light = '#9b59b6'

theme.system_cyan_dark = '#6BCDCE'
theme.system_cyan_light = '#6BCDCE'

theme.system_white_dark = '#f5f5f5'
theme.system_white_light = '#ffffff'

-- Accent color
theme.accent = theme.system_blue_dark

-- Background color
theme.background = '#000000' .. '66'

-- Transparent
theme.transparent = '#00000000'

-- Awesome icon
theme.awesome_icon = theme.icons .. 'awesome.svg'

local awesome_overrides = function(theme) end

return {
	theme = theme,
 	awesome_overrides = awesome_overrides
}
